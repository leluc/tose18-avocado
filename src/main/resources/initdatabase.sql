/*
Drop table if exists BUDGETS;
Drop table if exists CORPORATIONS ;
Drop table if exists COSTS ;
Drop table if exists DEPARTMENTS ;
Drop table if exists EMPLOYEES ;
Drop table if exists ITEMS ;
Drop table if exists LOCATIONS ;
Drop table if exists PROJECTMANAGEMENTS ;
Drop table if exists PROJECTS ;
Drop table if exists PROJECTTEAMS ;
Drop table if exists REPORTS ;
Drop table if exists STAKEHOLDERS ;
*/


create table if not exists items(
id long identity primary key,
name varchar(200),
quantity numeric(20));


create table if not exists departments(
depId long identity primary key,
depName varchar(200)
);

create table if not exists employees(
empId long identity primary key,
prename varchar(200),
lastname varchar(200),
empRole varchar(200),
depIdFk long,
foreign key (depIdFk) references departments (depId)
);

create table if not exists projects(
proId long identity primary key,
projectname varchar(200),
description varchar(200),
depIdFk long,
empIdFk long,
foreign key (depIdFk) references departments (depId),
foreign key (empIdFk) references employees (empId),
);

create table if not exists projectteams(
teamId long identity primary key,
proIdFk long,
empIdFk long,
foreign key (proIdFk) references projects (proId),
foreign key (empIdFk) references employees (empId)
);


create table if not exists locations(
locId long identity primary key,
locName varchar(200),
zipcode long
);

create table if not exists corporations(
corpId long identity primary key,
corpNumber numeric(20),
corpName varchar(200),
streetName varchar(200),
streetNumber numeric(100),
locIdFk long,
-- foreign key (locIdFk) references locations (locId)
);


create table if not exists stakeholders(
staId long identity primary key,
staName varchar(200),
staDescription varchar(200),
comment varchar(200),
corpIdFk long,
StaRole varchar(200),
-- foreign key (corpIdFk) references corporations (corpId)
);


create table if not exists budgets(
budId long identity primary key,
budAmount long,
staIdFk long,
proIdFk long,
-- foreign key (staIdFk) references stakeholders (staId),
-- foreign key (proIdFk) references projects (proId)
);

create table if not exists costs(
costId long identity primary key,
budIdFk long,
corpIdFk long,
amount numeric(10),
fallingdue date,
-- foreign key (budIdFk) references budgets (budId),
-- foreign key (corpIdFk) references corporations (corpId)
);

create table if not exists reports(
repId long identity primary key,
repName varchar(200),
proIdFk long, 
corpIdFk long, 
repGoal varchar(200), 
repDescription varchar(1000),
budIdFk long, 
teamIdFk long, 
staIdFk long,
-- foreign key (proIdFk) references projects (proId),
-- foreign key (corpIdFk) references corporations (corpId),
-- foreign key (budIdFk) references budgets (budId),
-- foreign key (teamIdFk) references projectteams (teamId),
-- foreign key (staIdFk) references stakeholders (staId),
);

create table if not exists projectmanagements(
manId long identity primary key,
manPriority long,
repIdFk long,
budIdFk long,
teamIdFk long,
-- foreign key (repIdFk) references reports (repId),
-- foreign key (budIdFk) references budgets (budId),
-- foreign key (teamIdFk) references projectteams (teamId),
);
