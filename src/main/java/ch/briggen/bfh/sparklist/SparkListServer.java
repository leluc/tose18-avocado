package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.ItemDeleteController;
import ch.briggen.bfh.sparklist.web.ItemEditController;
import ch.briggen.bfh.sparklist.web.ItemNewController;
import ch.briggen.bfh.sparklist.web.ItemUpdateController;

import ch.briggen.bfh.sparklist.web.EmployeeDeleteController;
import ch.briggen.bfh.sparklist.web.EmployeeEditController;
import ch.briggen.bfh.sparklist.web.EmployeeNewController;
import ch.briggen.bfh.sparklist.web.EmployeeUpdateController;
import ch.briggen.bfh.sparklist.web.EmployeeRootController;

import ch.briggen.bfh.sparklist.web.ProjectDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectEditController;
import ch.briggen.bfh.sparklist.web.ProjectNewController;
import ch.briggen.bfh.sparklist.web.ProjectUpdateController;
import ch.briggen.bfh.sparklist.web.ProjectRootController;

import ch.briggen.bfh.sparklist.web.StakeholderDeleteController;
import ch.briggen.bfh.sparklist.web.StakeholderEditController;
import ch.briggen.bfh.sparklist.web.StakeholderNewController;
import ch.briggen.bfh.sparklist.web.StakeholderUpdateController;
import ch.briggen.bfh.sparklist.web.StakeholderRootController;

import ch.briggen.bfh.sparklist.web.DepartmentEditController;
import ch.briggen.bfh.sparklist.web.DepartmentUpdateController;
import ch.briggen.bfh.sparklist.web.DepartmentDeleteController;
import ch.briggen.bfh.sparklist.web.DepartmentNewController;
import ch.briggen.bfh.sparklist.web.DepartmentRootController;

import ch.briggen.bfh.sparklist.web.ProjectTeamEditController;
import ch.briggen.bfh.sparklist.web.ProjectTeamUpdateController;
import ch.briggen.bfh.sparklist.web.ProjectTeamDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectTeamNewController;
import ch.briggen.bfh.sparklist.web.ProjectTeamRootController;

import ch.briggen.bfh.sparklist.web.CorporationEditController;
import ch.briggen.bfh.sparklist.web.CorporationUpdateController;
import ch.briggen.bfh.sparklist.web.CorporationDeleteController;
import ch.briggen.bfh.sparklist.web.CorporationNewController;
import ch.briggen.bfh.sparklist.web.CorporationRootController;

import ch.briggen.bfh.sparklist.web.LocationEditController;
import ch.briggen.bfh.sparklist.web.LocationUpdateController;
import ch.briggen.bfh.sparklist.web.LocationDeleteController;
import ch.briggen.bfh.sparklist.web.LocationNewController;
import ch.briggen.bfh.sparklist.web.LocationRootController;

import ch.briggen.bfh.sparklist.web.BudgetEditController;
import ch.briggen.bfh.sparklist.web.BudgetUpdateController;
import ch.briggen.bfh.sparklist.web.BudgetDeleteController;
import ch.briggen.bfh.sparklist.web.BudgetNewController;
import ch.briggen.bfh.sparklist.web.BudgetRootController;

import ch.briggen.bfh.sparklist.web.CostEditController;
import ch.briggen.bfh.sparklist.web.CostUpdateController;
import ch.briggen.bfh.sparklist.web.CostDeleteController;
import ch.briggen.bfh.sparklist.web.CostNewController;
import ch.briggen.bfh.sparklist.web.CostRootController;

import ch.briggen.bfh.sparklist.web.ReportEditController;
import ch.briggen.bfh.sparklist.web.ReportUpdateController;
import ch.briggen.bfh.sparklist.web.ReportDeleteController;
import ch.briggen.bfh.sparklist.web.ReportNewController;
import ch.briggen.bfh.sparklist.web.ReportRootController;

import ch.briggen.bfh.sparklist.web.ProjectManagementEditController;
import ch.briggen.bfh.sparklist.web.ProjectManagementUpdateController;
import ch.briggen.bfh.sparklist.web.ProjectManagementDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectManagementNewController;
import ch.briggen.bfh.sparklist.web.ProjectManagementRootController;

import ch.briggen.bfh.sparklist.web.ListManagementRootController;

import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		
		get("/item", new ItemEditController(), new UTF8ThymeleafTemplateEngine());
		post("/item/update", new ItemUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/item/delete", new ItemDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/item/new", new ItemNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/employee/list", new EmployeeRootController(), new UTF8ThymeleafTemplateEngine());
		get("/employee", new EmployeeEditController(), new UTF8ThymeleafTemplateEngine());
		post("/employee/update", new EmployeeUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/employee/delete", new EmployeeDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/employee/new", new EmployeeNewController(), new UTF8ThymeleafTemplateEngine());

		get("/project/list", new ProjectRootController(), new UTF8ThymeleafTemplateEngine());
		get("/project", new ProjectEditController(), new UTF8ThymeleafTemplateEngine());
		post("/project/update", new ProjectUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/project/delete", new ProjectDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/project/new", new ProjectNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/stakeholder/list", new StakeholderRootController(), new UTF8ThymeleafTemplateEngine());
		get("/stakeholder", new StakeholderEditController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholder/update", new StakeholderUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/stakeholder/delete", new StakeholderDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/stakeholder/new", new StakeholderNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/department/list", new DepartmentRootController(), new UTF8ThymeleafTemplateEngine());
		get("/department", new DepartmentEditController(), new UTF8ThymeleafTemplateEngine());
		post("/department/update", new DepartmentUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/department/delete", new DepartmentDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/department/new", new DepartmentNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/projectTeam/list", new ProjectTeamRootController(), new UTF8ThymeleafTemplateEngine());
		get("/projectTeam", new ProjectTeamEditController(), new UTF8ThymeleafTemplateEngine());
		post("/projectTeam/update", new ProjectTeamUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/projectTeam/delete", new ProjectTeamDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/projectTeam/new", new ProjectTeamNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/corporation/list", new CorporationRootController(), new UTF8ThymeleafTemplateEngine());
		get("/corporation", new CorporationEditController(), new UTF8ThymeleafTemplateEngine());
		post("/corporation/update", new CorporationUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/corporation/delete", new CorporationDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/corporation/new", new CorporationNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/location/list", new LocationRootController(), new UTF8ThymeleafTemplateEngine());
		get("/location", new LocationEditController(), new UTF8ThymeleafTemplateEngine());
		post("/location/update", new LocationUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/location/delete", new LocationDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/location/new", new LocationNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/budget/list", new BudgetRootController(), new UTF8ThymeleafTemplateEngine());
		get("/budget", new BudgetEditController(), new UTF8ThymeleafTemplateEngine());
		post("/budget/update", new BudgetUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/budget/delete", new BudgetDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/budget/new", new BudgetNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/cost/list", new CostRootController(), new UTF8ThymeleafTemplateEngine());
		get("/cost", new CostEditController(), new UTF8ThymeleafTemplateEngine());
		post("/cost/update", new CostUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/cost/delete", new CostDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/cost/new", new CostNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/report/list", new ReportRootController(), new UTF8ThymeleafTemplateEngine());
		get("/report", new ReportEditController(), new UTF8ThymeleafTemplateEngine());
		post("/report/update", new ReportUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/report/delete", new ReportDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/report/new", new ReportNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/projectManagement/list", new ProjectManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/projectManagement", new ProjectManagementEditController(), new UTF8ThymeleafTemplateEngine());
		post("/projectManagement/update", new ProjectManagementUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/projectManagement/delete", new ProjectManagementDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/projectManagement/new", new ProjectManagementNewController(), new UTF8ThymeleafTemplateEngine());
	}

}
