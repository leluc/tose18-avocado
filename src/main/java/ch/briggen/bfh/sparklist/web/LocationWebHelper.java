package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Location;
import spark.Request;

class LocationWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(LocationWebHelper.class);
	
	public static Location locationFromWeb(Request request)
	{
		return new Location(
				Long.parseLong(request.queryParams("locationDetail.locId")),				
				request.queryParams("locationDetail.locName"),
				Long.parseLong(request.queryParams("locationDetail.zipcode")));
	}

}