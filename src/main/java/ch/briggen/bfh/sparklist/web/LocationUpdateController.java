package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Location;
import ch.briggen.bfh.sparklist.domain.LocationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Location
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class LocationUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(LocationUpdateController.class);
		
	private LocationRepository locationRepo = new LocationRepository();
	


	/**
	 * Schreibt das geänderte Location zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/location) mit der Corporation-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /location/update
	 * 
	 * @return redirect nach /location: via Browser wird /location aufgerufen, also editLocation weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Location locationDetail = LocationWebHelper.locationFromWeb(request);
		
		log.trace("POST /location/update mit locationDetail " + locationDetail);
		
		//Speichern des Locations in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /location&corpId=3 (wenn locationDetail.getLocId == 3 war)
		locationRepo.save(locationDetail);
		response.redirect("/location?corpId="+ locationDetail.getLocId());
		return null;
	}
}


