package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Employee;
import spark.Request;

class EmployeeWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(EmployeeWebHelper.class);
	
	public static Employee employeeFromWeb(Request request)
	{
		return new Employee(
				Long.parseLong(request.queryParams("employeeDetail.empId")),
				request.queryParams("employeeDetail.prename"),
				request.queryParams("employeeDetail.lastname"),
				request.queryParams("employeeDetail.empRole"),
				Long.parseLong(request.queryParams("employeeDetail.depIdFk")));
	}

}
