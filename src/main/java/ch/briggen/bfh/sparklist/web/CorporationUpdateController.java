package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Corporation;
import ch.briggen.bfh.sparklist.domain.CorporationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class CorporationUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(CorporationUpdateController.class);
		
	private CorporationRepository corporationRepo = new CorporationRepository();
	


	/**
	 * Schreibt das geänderte Corporation zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/corporation) mit der Corporation-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /corporation/update
	 * 
	 * @return redirect nach /corporation: via Browser wird /corporation aufgerufen, also editCorporation weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Corporation corporationDetail = CorporationWebHelper.corporationFromWeb(request);
		
		log.trace("POST /corporation/update mit corporationDetail " + corporationDetail);
		
		//Speichern des Corporations in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /corporation&corpId=3 (wenn corporationDetail.getCorpId == 3 war)
		corporationRepo.save(corporationDetail);
		response.redirect("/corporation?corpId="+corporationDetail.getCorpId());
		return null;
	}
}


