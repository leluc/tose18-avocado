package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class EmployeeRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(EmployeeRootController.class);
	

	EmployeeRepository repositoryEmployee = new EmployeeRepository();
	

	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("listEmployee", repositoryEmployee.getAll());
		return new ModelAndView(model, "employeeListTemplate");
	}
}
