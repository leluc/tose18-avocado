package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import spark.Request;

class CostWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(CostWebHelper.class);
	
	public static Cost costFromWeb(Request request)
	{
		return new Cost(
				Long.parseLong(request.queryParams("costDetail.costId")),
				Long.parseLong(request.queryParams("costDetail.budIdFk")),
				Long.parseLong(request.queryParams("costDetail.corpIdFk")),
				Integer.parseInt(request.queryParams("costDetail.amount")),
				request.queryParams("costDetail.fallingdue")
				);
	}
	}