package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectManagement;
import ch.briggen.bfh.sparklist.domain.ProjectManagementRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen ProjectManagements !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class ProjectManagementEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ProjectManagementEditController.class);

	private ProjectManagementRepository projectManagementRepo = new ProjectManagementRepository();

	/**
	 * Requesthandler zum Bearbeiten eines ProjectManagement. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues ProjectManagement erstellt (Aufruf von
	 * /projectManagement/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das ProjectManagement mit der übergebenen id upgedated (Aufruf
	 * /projectManagement/save) Hört auf GET /budget
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "projectManagementDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String manIdString = request.queryParams("manId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		// TODO: check if 0 or null
		if (null == manIdString) {
			log.trace("GET /projectManagement für INSERT mit manId " + manIdString);
			// der Submit-Button ruft /projectManagement/new auf --> INSERT
			model.put("postAction", "/projectManagement/new");
			model.put("projectManagementDetail", new ProjectManagement());

		} else {
			log.trace("GET /projectManagement für UPDATE mit manId " + manIdString);
			// der Submit-Button ruft /budget/update auf --> UPDATE
			model.put("postAction", "/budget/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "budgetDetail" hinzugefügt. budgetDetail muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long manId = Long.parseLong(manIdString);
			ProjectManagement i = projectManagementRepo.getByManId(manId);
			model.put("projectManagementDetail", i);
		}

		// das Template budgetDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projectManagementDetailTemplate");
	}

}
