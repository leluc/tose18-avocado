package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class EmployeeUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeUpdateController.class);
		
	private EmployeeRepository employeeRepo = new EmployeeRepository();
	


	/**
	 * Schreibt das geänderte Employee zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/employee) mit der Employee-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /employee/update
	 * 
	 * @return redirect nach /employee: via Browser wird /employee aufgerufen, also editEmployee weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Employee employeeDetail = EmployeeWebHelper.employeeFromWeb(request);
		
		log.trace("POST /employee/update mit employeeDetail " + employeeDetail);
		
		//Speichern des Employees in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /employee&id=3 (wenn employeeDetail.getId == 3 war)
		employeeRepo.save(employeeDetail);
		response.redirect("/employee/list");
		return null;
	}
}


