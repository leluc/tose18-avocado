package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Report;
import ch.briggen.bfh.sparklist.domain.ReportRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Reports
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ReportUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ReportUpdateController.class);
		
	private ReportRepository reportRepo = new ReportRepository();
	


	/**
	 * Schreibt das geänderte Report zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/report) mit der Employee-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /report/update
	 * 
	 * @return redirect nach /report: via Browser wird /report aufgerufen, also editDepartment weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Report reportDetail = ReportWebHelper.reportFromWeb(request);
		
		log.trace("POST /report/update mit reportDetail " + reportDetail);
		
		//Speichern des Reports in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /report&depId=3 (wenn reportDetail.getRepId == 3 war)
		reportRepo.save(reportDetail);
		response.redirect("/report?empId="+reportDetail.getRepId());
		return null;
	}
}


