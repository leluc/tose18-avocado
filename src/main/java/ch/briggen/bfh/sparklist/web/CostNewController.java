package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Costs
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class CostNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(CostNewController.class);
		
	private CostRepository costRepo = new CostRepository();
	
	/**
	 * Erstellt ein neues Cost in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /cost&depId=99  wenn die costId 99 war.)
	 * 
	 * Hört auf POST /cost/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Cost costDetail = CostWebHelper.costFromWeb(request);
		log.trace("POST /cost/new mit costDetail " + costDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long costId = costRepo.insert(costDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /cost?depId=432932
		response.redirect("/cost?depId="+costId);
		return null;
	}
}


