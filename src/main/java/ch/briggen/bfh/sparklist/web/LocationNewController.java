package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Location;
import ch.briggen.bfh.sparklist.domain.LocationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Locations
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class LocationNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(LocationNewController.class);
		
	private LocationRepository locationRepo = new LocationRepository();
	
	/**
	 * Erstellt ein neues Location in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /location&corpId=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /location/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Location locationDetail = LocationWebHelper.locationFromWeb(request);
		log.trace("POST /location/new mit locationDetail " + locationDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long locId = locationRepo.insert(locationDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /location?id=432932
		response.redirect("/location?corpId="+locId);
		return null;
	}
}


