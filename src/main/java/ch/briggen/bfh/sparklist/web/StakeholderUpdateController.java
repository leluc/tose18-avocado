package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Stakeholder
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class StakeholderUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderUpdateController.class);
		
	private StakeholderRepository stakeholderRepo = new StakeholderRepository();
	


	/**
	 * Schreibt der geänderte Stakeholder zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/stakeholder) mit der Stakeholder-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /stakeholder/update
	 * 
	 * @return redirect nach /stakeholder: via Browser wird /stakeholder aufgerufen, also editStakeholder weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Stakeholder stakeholderDetail = StakeholderWebHelper.stakeholderFromWeb(request);
		
		log.trace("POST /stakeholder/update mit stakeholderDetail " + stakeholderDetail);
		
		//Speichern des Stakeholder in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /stakeholder&staId=3 (wenn stakeholderDetail.getStaId == 3 war)
		stakeholderRepo.save(stakeholderDetail);
		response.redirect("/stakeholder/list");
		return null;
	}
}


