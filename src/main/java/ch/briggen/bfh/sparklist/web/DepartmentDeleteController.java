package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Department
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class DepartmentDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(DepartmentDeleteController.class);
		
	private DepartmentRepository departmentRepo = new DepartmentRepository();
	

	/**
	 * Löscht das Department mit der übergebenen id in der Datenbank
	 * /department/delete&depId=987 löscht das Department mit der depId 987 aus der Datenbank
	 * 
	 * Hört auf GET /department/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String depId = request.queryParams("depId");
		log.trace("GET /department/delete mit depId " + depId);
		
		Long longDepId = Long.parseLong(depId);
		departmentRepo.delete(longDepId);
		response.redirect("/department/list");
		return null;
	}
}


