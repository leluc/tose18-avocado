package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class EmployeeDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeDeleteController.class);
		
	private EmployeeRepository employeeRepo = new EmployeeRepository();
	

	/**
	 * Löscht das Employee mit der übergebenen id in der Datenbank
	 * /employee/delete&id=987 löscht das Employee mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /employee/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String empId = request.queryParams("empId");
		log.trace("GET /employee/delete mit empId " + empId);
		
		Long longEmpId = Long.parseLong(empId);
		employeeRepo.delete(longEmpId);
		response.redirect("/employee/list");
		return null;
	}
}


