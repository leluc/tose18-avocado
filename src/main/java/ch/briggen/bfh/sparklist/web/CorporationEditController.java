package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Corporation;
import ch.briggen.bfh.sparklist.domain.CorporationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Corporation !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class CorporationEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(CorporationEditController.class);

	private CorporationRepository corporationRepo = new CorporationRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Corporations. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Corporation erstellt (Aufruf von
	 * /corporation/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Employee mit der übergebenen id upgedated (Aufruf
	 * /corporation/save) Hört auf GET /employee
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "corporationDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String corpIdString = request.queryParams("corpId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		// TODO: check if 0 or null
		if (null == corpIdString) {
			log.trace("GET /corporation für INSERT mit corpId " + corpIdString);
			// der Submit-Button ruft /corporation/new auf --> INSERT
			model.put("postAction", "/corporation/new");
			model.put("corporationDetail", new Corporation());

		} else {
			log.trace("GET /corporation für UPDATE mit corpId " + corpIdString);
			// der Submit-Button ruft /corporation/update auf --> UPDATE
			model.put("postAction", "/corporation/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "employeeDetail" hinzugefügt. employeeDetal muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long corpId = Long.parseLong(corpIdString);
			Corporation i = corporationRepo.getByCorpId(corpId);
			model.put("corporationDetail", i);
		}

		// das Template corporationDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "corporationDetailTemplate");
	}

}
