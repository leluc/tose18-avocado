package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Projects
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectNewController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	
	/**
	 * Erstellt ein neues Project in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /project&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /project/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Project projectDetail = ProjectWebHelper.projectFromWeb(request);
		log.trace("POST /project/new mit projectDetail " + projectDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long proId = projectRepo.insert(projectDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /project?proId=432932
		response.redirect("/");
		return null;
	}
}


