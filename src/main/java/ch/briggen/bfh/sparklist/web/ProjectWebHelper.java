package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import spark.Request;

class ProjectWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectWebHelper.class);

	public static Project projectFromWeb(Request request) {
		return new Project(
				Long.parseLong(request.queryParams("projectDetail.proId")),
				request.queryParams("projectDetail.projectname"), 
				request.queryParams("projectDetail.description"),
				Long.parseLong(request.queryParams("projectDetail.depIdFk")),
				Long.parseLong(request.queryParams("projectDetail.empIdFk")));
	}

}
