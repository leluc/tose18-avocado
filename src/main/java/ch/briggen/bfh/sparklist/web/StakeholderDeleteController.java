package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelne Stakeholder
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class StakeholderDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderDeleteController.class);
		
	private StakeholderRepository stakeholderRepo = new StakeholderRepository();
	

	/**
	 * Löscht den Stakeholder mit der übergebenen staId in der Datenbank
	 * /stakeholder/delete&staId=987 löscht den Stakeholder mit der StaId 987 aus der Datenbank
	 * 
	 * Hört auf GET /stakeholder/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String staId = request.queryParams("staId");
		log.trace("GET /stakeholder/delete mit staId " + staId);
		
		Long longStaId = Long.parseLong(staId);
		stakeholderRepo.delete(longStaId);
		response.redirect("/stakeholder/list");
		return null;
	}
}


