package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Corporation;
import ch.briggen.bfh.sparklist.domain.CorporationRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class CorporationRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(CorporationRootController.class);
	

	CorporationRepository repositoryCorporation = new CorporationRepository();
	

	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("listCorporation", repositoryCorporation.getAll());
		return new ModelAndView(model, "corporationListTemplate");
	}
}
