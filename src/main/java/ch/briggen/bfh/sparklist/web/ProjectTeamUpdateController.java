package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectTeam;
import ch.briggen.bfh.sparklist.domain.ProjectTeamRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectTeamUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ProjectTeamUpdateController.class);
		
	private ProjectTeamRepository projectTeamRepo = new ProjectTeamRepository();
	


	/**
	 * Schreibt das geänderte ProjectTeam zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/projectTeam) mit der Employee-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /department/update
	 * 
	 * @return redirect nach /projectTeamt: via Browser wird /employee aufgerufen, also editDepartment weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		ProjectTeam projectTeamDetail = ProjectTeamWebHelper.projectTeamFromWeb(request);
		
		log.trace("POST /projectTeam/update mit projectTeamDetail " + projectTeamDetail);
		
		//Speichern des ProjectTeam in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /ProjectTeam&dteamId=3 (wenn projectTeamDetail.getTeamId == 3 war)
		projectTeamRepo.save(projectTeamDetail);
		response.redirect("/projectTeam/list");
		return null;
	}
}


