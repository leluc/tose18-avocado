package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectManagement;
import ch.briggen.bfh.sparklist.domain.ProjectManagementRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen ProjectManagements
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectManagementNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectManagementNewController.class);
		
	private ProjectManagementRepository projectManagementRepo = new ProjectManagementRepository();
	
	/**
	 * Erstellt ein neues ProjectManagement in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /projectManagement&depId=99  wenn die budId 99 war.)
	 * 
	 * Hört auf POST /projectManagement/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		ProjectManagement projectManagementDetail = ProjectManagementWebHelper.projectManagementFromWeb(request);
		log.trace("POST /projectManagement/new mit projectManagementDetail " + projectManagementDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long manId = projectManagementRepo.insert(projectManagementDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /projectManagement?depId=432932
		response.redirect("/projectManagement?manId="+ manId);
		return null;
	}
}


