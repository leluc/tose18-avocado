package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Department;
import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Employees !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class EmployeeEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(EmployeeEditController.class);

	private EmployeeRepository employeeRepo = new EmployeeRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Employees. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Employee erstellt (Aufruf von
	 * /employee/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Employee mit der übergebenen id upgedated (Aufruf
	 * /employee/save) Hört auf GET /employee
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "employeeDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String empIdString = request.queryParams("empId");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		DepartmentRepository depRepo = new DepartmentRepository();
		Collection<Department> allDeps = depRepo.getAll();
		model.put("departments", allDeps);

		// TODO: check if 0 or null
		if (null == empIdString) {
			log.trace("GET /employee für INSERT mit empId " + empIdString);
			// der Submit-Button ruft /employee/new auf --> INSERT
			model.put("postAction", "/employee/new");
			model.put("employeeDetail", new Employee());

		} else {
			log.trace("GET /employee für UPDATE mit empId " + empIdString);
			// der Submit-Button ruft /employee/update auf --> UPDATE
			model.put("postAction", "/employee/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "employeeDetail" hinzugefügt. employeeDetal muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long empId = Long.parseLong(empIdString);
			Employee i = employeeRepo.getByEmpId(empId);
			model.put("employeeDetail", i);
		}

		// das Template employeeDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "employeeDetailTemplate");
	}

}
