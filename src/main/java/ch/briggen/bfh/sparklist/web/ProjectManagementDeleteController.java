package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectManagementRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen ProjectManagement
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectManagementDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectManagementDeleteController.class);
		
	private ProjectManagementRepository projectManagementRepo = new ProjectManagementRepository();
	

	/**
	 * Löscht das ProjectManagement mit der übergebenen id in der Datenbank
	 * /projectManagement/delete&id=987 löscht das projectManagement mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /projectManagement/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String manId = request.queryParams("manId");
		log.trace("GET /projectManagement/delete mit manId " + manId);
		
		Long longManId = Long.parseLong(manId);
		projectManagementRepo.delete(longManId);
		response.redirect("/");
		return null;
	}
}


