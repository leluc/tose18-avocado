package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Costs !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class CostEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(CostEditController.class);

	private CostRepository costRepo = new CostRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Cost. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Cost erstellt (Aufruf von
	 * /employee/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Cost mit der übergebenen id upgedated (Aufruf
	 * /employee/save) Hört auf GET /budget
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "costDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String costIdString = request.queryParams("costId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		// TODO: check if 0 or null
		if (null == costIdString) {
			log.trace("GET /cost für INSERT mit budId " + costIdString);
			// der Submit-Button ruft /budget/new auf --> INSERT
			model.put("postAction", "/cost/new");
			model.put("costDetail", new Cost());

		} else {
			log.trace("GET /cost für UPDATE mit costId " + costIdString);
			// der Submit-Button ruft /cost/update auf --> UPDATE
			model.put("postAction", "/cost/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "costDetail" hinzugefügt. costDetail muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long costId = Long.parseLong(costIdString);
			Cost i = costRepo.getByCostId(costId);
			model.put("costDetail", i);
		}

		// das Template costDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "costDetailTemplate");
	}

}
