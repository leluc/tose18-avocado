package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectTeam;
import ch.briggen.bfh.sparklist.domain.ProjectTeamRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectTeamRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ProjectTeamRootController.class);
	

	ProjectTeamRepository repositoryProjectTeam = new ProjectTeamRepository();
	

	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("listProjectTeam", repositoryProjectTeam.getAll());
		return new ModelAndView(model, "projectTeamListTemplate");
	}
}
