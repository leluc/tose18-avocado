package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Report;
import spark.Request;

class ReportWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ReportWebHelper.class);
	
	public static Report reportFromWeb(Request request)
	{
		return new Report(
				Long.parseLong(request.queryParams("reportDetail.repId")),
				request.queryParams("reportDetail.repName"),
				Long.parseLong(request.queryParams("reportDetail.proIdFk")),
				Long.parseLong(request.queryParams("reportDetail.corpIdFk")),
				request.queryParams("reportDetail.repGoal"),
				request.queryParams("reportDetail.repDescription"),
				Long.parseLong(request.queryParams("reportDetail.budIdFk")),
				Long.parseLong(request.queryParams("reportDetail.teamIdFk")),
				Long.parseLong(request.queryParams("reportDetail.staIdFk")));
	}
	}