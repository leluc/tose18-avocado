package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Projects
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ProjectUpdateController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	


	/**
	 * Schreibt das geänderte Project zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/project) mit der Project-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /project/update
	 * 
	 * @return redirect nach /project: via Browser wird /project aufgerufen, also editProject weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Project projectDetail = ProjectWebHelper.projectFromWeb(request);
		
		log.trace("POST /project/update mit projectDetail " + projectDetail);
		
		//Speichern des Projects in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /project&id=3 (wenn projectDetail.getProId == 3 war)
		projectRepo.save(projectDetail);
		response.redirect("/");
		return null;
	}
}


