package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Report;
import ch.briggen.bfh.sparklist.domain.ReportRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ReportRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ReportRootController.class);
	

	ReportRepository repositoryReport = new ReportRepository();
	

	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("listReport", repositoryReport.getAll());
		return new ModelAndView(model, "ReportListTemplate");
	}
}
