package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Employees !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class BudgetEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(BudgetEditController.class);

	private BudgetRepository budgetRepo = new BudgetRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Budget. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Budget erstellt (Aufruf von
	 * /employee/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Budget mit der übergebenen id upgedated (Aufruf
	 * /employee/save) Hört auf GET /budget
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "budgetDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String budIdString = request.queryParams("budId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		ProjectRepository proRepo = new ProjectRepository();
		Collection<Project> allPros = proRepo.getAll();
		model.put("projects", allPros);
		
		StakeholderRepository staRepo = new StakeholderRepository();
		Collection<Stakeholder> allStas = staRepo.getAll();
		model.put("stakeholders", allStas);
		
		
		// TODO: check if 0 or null
		if (null == budIdString) {
			log.trace("GET /budget für INSERT mit budId " + budIdString);
			// der Submit-Button ruft /budget/new auf --> INSERT
			model.put("postAction", "/budget/new");
			model.put("budgetDetail", new Budget());

		} else {
			log.trace("GET /budget für UPDATE mit budId " + budIdString);
			// der Submit-Button ruft /budget/update auf --> UPDATE
			model.put("postAction", "/budget/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "budgetDetail" hinzugefügt. budgetDetail muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long budId = Long.parseLong(budIdString);
			Budget i = budgetRepo.getByBudId(budId);
			log.info(budgetRepo.toString());
			model.put("budgetDetail", i);
		}

		// das Template budgetDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "budgetDetailTemplate");
	}

}
