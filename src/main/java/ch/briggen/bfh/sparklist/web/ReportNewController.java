package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Report;
import ch.briggen.bfh.sparklist.domain.ReportRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Reports
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ReportNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ReportNewController.class);
		
	private ReportRepository reportRepo = new ReportRepository();
	
	/**
	 * Erstellt ein neues Report in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /report&depId=99  wenn die repId 99 war.)
	 * 
	 * Hört auf POST /report/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Report reportDetail = ReportWebHelper.reportFromWeb(request);
		log.trace("POST /report/new mit reportDetail " + reportDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long repId = reportRepo.insert(reportDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /report?repId=432932
		response.redirect("/report?repId="+repId);
		return null;
	}
}


