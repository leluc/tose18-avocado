package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Budgets
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class BudgetUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(BudgetUpdateController.class);
		
	private BudgetRepository budgetRepo = new BudgetRepository();
	


	/**
	 * Schreibt das geänderte Budget zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/budget) mit der Budget-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /budget/update
	 * 
	 * @return redirect nach /budget: via Browser wird /budget aufgerufen, also editBudget weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Budget budgetDetail = BudgetWebHelper.budgetFromWeb(request);
		
		log.trace("POST /budget/update mit budgetDetail " + budgetDetail);
		
		//Speichern des Budgets in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /budget&depId=3 (wenn budgetDetail.getBudId == 3 war)
		budgetRepo.save(budgetDetail);
		response.redirect("/budget/list");
		return null;
	}
}


