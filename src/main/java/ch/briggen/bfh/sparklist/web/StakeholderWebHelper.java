package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Stakeholder;
import spark.Request;

class StakeholderWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(StakeholderWebHelper.class);
	
	public static Stakeholder stakeholderFromWeb(Request request) {
		return new Stakeholder(
				Long.parseLong(request.queryParams("stakeholderDetail.staId")),
				request.queryParams("stakeholderDetail.staName"),
				request.queryParams("stakeholderDetail.staDescription"),
				request.queryParams("stakeholderDetail.comment"));
	}

}
