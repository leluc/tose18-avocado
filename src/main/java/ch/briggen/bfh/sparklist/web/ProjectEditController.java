package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Department;
import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Projects !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class ProjectEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ProjectEditController.class);

	private ProjectRepository projectRepo = new ProjectRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Projects. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Project erstellt (Aufruf von
	 * /project/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Project mit der übergebenen id upgedated (Aufruf
	 * /project/save) Hört auf GET /project
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "projectDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String proIdString = request.queryParams("proId");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		DepartmentRepository depRepo = new DepartmentRepository();
		Collection<Department> allDeps = depRepo.getAll();
		model.put("departments", allDeps);
		
		EmployeeRepository empRepo = new EmployeeRepository();
		Collection<Employee> allEmps = empRepo.getAll();
		model.put("employees", allEmps);


		// TODO: check if 0 or null
		if (null == proIdString) {
			log.trace("GET /project für INSERT mit proId " + proIdString);
			// der Submit-Button ruft /project/new auf --> INSERT
			model.put("postAction", "/project/new");
			model.put("projectDetail", new Project());

		} else {
			log.trace("GET /project für UPDATE mit proId " + proIdString);
			// der Submit-Button ruft /project/update auf --> UPDATE
			model.put("postAction", "/project/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "projectDetail" hinzugefügt. projectDetal muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long proId = Long.parseLong(proIdString);
			Project i = projectRepo.getByProId(proId);
			model.put("projectDetail", i);
		}

		// das Template projectDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projectDetailTemplate");
	}

}
