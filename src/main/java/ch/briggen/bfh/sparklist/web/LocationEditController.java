package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Location;
import ch.briggen.bfh.sparklist.domain.LocationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Location !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class LocationEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(LocationEditController.class);

	private LocationRepository locationRepo = new LocationRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Locations. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Corporation erstellt (Aufruf von
	 * /location/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Employee mit der übergebenen id upgedated (Aufruf
	 * /location/save) Hört auf GET /employee
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "corporationDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String locIdString = request.queryParams("locId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		// TODO: check if 0 or null
		if (null == locIdString) {
			log.trace("GET /location für INSERT mit locId " + locIdString);
			// der Submit-Button ruft /location/new auf --> INSERT
			model.put("postAction", "/location/new");
			model.put("locationDetail", new Location());

		} else {
			log.trace("GET /location für UPDATE mit locId " + locIdString);
			// der Submit-Button ruft /location/update auf --> UPDATE
			model.put("postAction", "/location/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "employeeDetail" hinzugefügt. employeeDetal muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long locId = Long.parseLong(locIdString);
			Location i = locationRepo.getByLocId(locId);
			model.put("locationDetail", i);
		}

		// das Template corporationDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "locationDetailTemplate");
	}

}
