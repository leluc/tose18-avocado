package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Budget
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class BudgetDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(BudgetDeleteController.class);
		
	private BudgetRepository budgetRepo = new BudgetRepository();
	

	/**
	 * Löscht das Budget mit der übergebenen id in der Datenbank
	 * /budget/delete&id=987 löscht das Budget mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /budget/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String budId = request.queryParams("budId");
		log.trace("GET /budget/delete mit budId " + budId);
		
		Long longBudId = Long.parseLong(budId);
		budgetRepo.delete(longBudId);
		response.redirect("/budget/list");
		return null;
	}
}


