package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectManagement;
import ch.briggen.bfh.sparklist.domain.ProjectManagementRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class ProjectManagementRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ProjectManagementRootController.class);
	

	ProjectManagementRepository repositoryProjectManagement = new ProjectManagementRepository();
	

	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("listProjectManagement", repositoryProjectManagement.getAll());
		return new ModelAndView(model, "projectManagementListTemplate");
	}
}
