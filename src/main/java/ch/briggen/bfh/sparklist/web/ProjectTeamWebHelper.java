package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectTeam;
import spark.Request;

class ProjectTeamWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectTeamWebHelper.class);
	
	public static ProjectTeam projectTeamFromWeb(Request request)
	{
		return new ProjectTeam(
				Long.parseLong(request.queryParams("projectTeamDetail.teamId")),				
				Long.parseLong(request.queryParams("projectTeamDetail.proIdFk")),
				Long.parseLong(request.queryParams("projectTeamDetail.empIdFk"))
				);
	}
}