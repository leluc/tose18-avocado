package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Corporation;
import ch.briggen.bfh.sparklist.domain.CorporationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Corporations
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class CorporationNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(CorporationNewController.class);
		
	private CorporationRepository corporationRepo = new CorporationRepository();
	
	/**
	 * Erstellt ein neues Corporation in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /corporation&corpId=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /corporation/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Corporation corporationDetail = CorporationWebHelper.corporationFromWeb(request);
		log.trace("POST /corporation/new mit corporationDetail " + corporationDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long corpId = corporationRepo.insert(corporationDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /employee?id=432932
		response.redirect("/corporation?corpId="+corpId);
		return null;
	}
}


