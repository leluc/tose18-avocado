package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.CostRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Budget
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class CostDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(CostDeleteController.class);
		
	private CostRepository costRepo = new CostRepository();
	

	/**
	 * Löscht das cost mit der übergebenen id in der Datenbank
	 * /cost/delete&id=987 löscht das cost mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /cost/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String costId = request.queryParams("costId");
		log.trace("GET /budget/delete mit costId " + costId);
		
		Long longCostId = Long.parseLong(costId);
		costRepo.delete(longCostId);
		response.redirect("/");
		return null;
	}
}


