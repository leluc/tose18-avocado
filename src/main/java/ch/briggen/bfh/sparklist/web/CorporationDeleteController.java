package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.CorporationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Corporation
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class CorporationDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(CorporationDeleteController.class);
		
	private CorporationRepository corporationRepo = new CorporationRepository();
	

	/**
	 * Löscht das Corporation mit der übergebenen id in der Datenbank
	 * /employee/delete&id=987 löscht das Corporation mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /corporation/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String corpId = request.queryParams("corpId");
		log.trace("GET /corporation/delete mit corpId " + corpId);
		
		Long longCorpId = Long.parseLong(corpId);
		corporationRepo.delete(longCorpId);
		response.redirect("/");
		return null;
	}
}


