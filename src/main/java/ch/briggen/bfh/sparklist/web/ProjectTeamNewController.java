package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectTeam;
import ch.briggen.bfh.sparklist.domain.ProjectTeamRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectTeamNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectTeamNewController.class);
		
	private ProjectTeamRepository projectTeamRepo = new ProjectTeamRepository();
	
	/**
	 * Erstellt ein neues ProjectTeam in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /projectTeamt&dteamId=99  wenn die teamId 99 war.)
	 * 
	 * Hört auf POST /projectTeam/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		ProjectTeam projectTeamDetail = ProjectTeamWebHelper.projectTeamFromWeb(request);
		log.trace("POST /projectTeam/new mit projectTeamDetail " + projectTeamDetail);
		
		//insert gibt die von der DB erstellte teamId zurück.
		long teamId = projectTeamRepo.insert(projectTeamDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /department?teamId=432932
		response.redirect("/projectTeam/list");
		return null;
	}
}


