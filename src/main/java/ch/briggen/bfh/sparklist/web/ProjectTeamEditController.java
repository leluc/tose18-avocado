package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Department;
import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import ch.briggen.bfh.sparklist.domain.ProjectTeam;
import ch.briggen.bfh.sparklist.domain.ProjectTeamRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen ProjectTeams !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class ProjectTeamEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ProjectTeamEditController.class);

	private ProjectTeamRepository projectTeamRepo = new ProjectTeamRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Department. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Department erstellt (Aufruf von
	 * /employee/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Employee mit der übergebenen id upgedated (Aufruf
	 * /employee/save) Hört auf GET /employee
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "departmentDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String teamIdString = request.queryParams("teamId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		ProjectRepository proRepo = new ProjectRepository();
		Collection<Project> allPros = proRepo.getAll();
		model.put("projects", allPros);
		
		EmployeeRepository empRepo = new EmployeeRepository();
		Collection<Employee> allEmps = empRepo.getAll();
		model.put("employees", allEmps);
		
		// TODO: check if 0 or null
		if (null == teamIdString) {
			log.trace("GET /projectTeam für INSERT mit teamId " + teamIdString);			
			// der Submit-Button ruft /department/new auf --> INSERT
			model.put("postAction", "/projectTeam/new");
			model.put("projectTeamDetail", new ProjectTeam());

		} else {
			// der Submit-Button ruft /projectTeam/update auf --> UPDATE
			model.put("postAction", "/projectTeam/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "departmentDetail" hinzugefügt. departmentDetail muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long teamId = Long.parseLong(teamIdString);
			ProjectTeam i = projectTeamRepo.getByTeamId(teamId);
			log.info(projectTeamRepo.toString());
			model.put("projectTeamDetail", i);
		}

		// das Template departmentDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projectTeamDetailTemplate");
	}

}
