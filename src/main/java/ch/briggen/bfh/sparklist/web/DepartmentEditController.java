package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Department;
import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Employees !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class DepartmentEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(DepartmentEditController.class);

	private DepartmentRepository departmentRepo = new DepartmentRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Department. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Department erstellt (Aufruf von
	 * /employee/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Employee mit der übergebenen id upgedated (Aufruf
	 * /employee/save) Hört auf GET /employee
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "departmentDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String depIdString = request.queryParams("depId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		// TODO: check if 0 or null
		if (null == depIdString) {
			log.trace("GET /department für INSERT mit depId " + depIdString);
			// der Submit-Button ruft /department/new auf --> INSERT
			model.put("postAction", "/department/new");
			model.put("departmentDetail", new Department());

		} else {
			log.trace("GET /department für UPDATE mit depId " + depIdString);
			// der Submit-Button ruft /department/update auf --> UPDATE
			model.put("postAction", "/department/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "departmentDetail" hinzugefügt. departmentDetail muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long depId = Long.parseLong(depIdString);
			Department i = departmentRepo.getByDepId(depId);
			log.info(departmentRepo.toString());
			model.put("departmentDetail", i);
		}

		// das Template departmentDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "departmentDetailTemplate");
	}

}
