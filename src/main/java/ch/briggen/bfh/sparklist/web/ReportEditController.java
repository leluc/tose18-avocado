package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Report;
import ch.briggen.bfh.sparklist.domain.ReportRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Reports !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class ReportEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(ReportEditController.class);

	private ReportRepository reportRepo = new ReportRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Report. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neues Report erstellt (Aufruf von
	 * /report/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Report mit der übergebenen id upgedated (Aufruf
	 * /report/save) Hört auf GET /employee
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "reportDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String repIdString = request.queryParams("repId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		// TODO: check if 0 or null
		if (null == repIdString) {
			log.trace("GET /report für INSERT mit depId " + repIdString);
			// der Submit-Button ruft /report/new auf --> INSERT
			model.put("postAction", "/report/new");
			model.put("reportDetail", new Report());

		} else {
			log.trace("GET /report für UPDATE mit repId " + repIdString);
			// der Submit-Button ruft /report/update auf --> UPDATE
			model.put("postAction", "/report/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "reportDetail" hinzugefügt. reportDetail muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long repId = Long.parseLong(repIdString);
			Report i = reportRepo.getByRepId(repId);
			model.put("reportDetail", i);
		}

		// das Template reportDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "reportDetailTemplate");
	}

}
