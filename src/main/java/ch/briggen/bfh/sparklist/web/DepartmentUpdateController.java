package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Department;
import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class DepartmentUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(DepartmentUpdateController.class);
		
	private DepartmentRepository departmentRepo = new DepartmentRepository();
	


	/**
	 * Schreibt das geänderte Department zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/department) mit der Employee-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /department/update
	 * 
	 * @return redirect nach /department: via Browser wird /employee aufgerufen, also editDepartment weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Department departmentDetail = DepartmentWebHelper.departmentFromWeb(request);
		
		log.trace("POST /employee/update mit departmentDetail " + departmentDetail);
		
		//Speichern des Employees in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /department&depId=3 (wenn departmentDetail.getDepId == 3 war)
		departmentRepo.save(departmentDetail);
		response.redirect("/department/list");
		return null;
	}
}


