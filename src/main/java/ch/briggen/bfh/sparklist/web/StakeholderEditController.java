package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Stakeholder !!! Diese Version
 * verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Marcel Briggen
 *
 */

public class StakeholderEditController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(StakeholderEditController.class);

	private StakeholderRepository stakeholderRepo = new StakeholderRepository();

	/**
	 * Requesthandler zum Bearbeiten eines Stakeholder. Liefert das Formular (bzw.
	 * Template) zum bearbeiten der einzelnen Felder Wenn der id Parameter 0 ist
	 * wird beim submitten des Formulars ein neuer Stakeholder erstellt (Aufruf von
	 * /stakeholder/new) Wenn der id Parameter <> 0 ist wird beim submitten des
	 * Formulars das Stakeholder mit der übergebenen id upgedated (Aufruf
	 * /stakeholder/save) Hört auf GET /stakeholder
	 * 
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer
	 *         "stakeholderDetailTemplate" .
	 */

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String staIdString = request.queryParams("staId");
		HashMap<String, Object> model = new HashMap<String, Object>();

		// TODO: check if 0 or null
		if (null == staIdString) {
			log.trace("GET /stakeholder für INSERT mit staId " + staIdString);
			// der Submit-Button ruft /stakeholder/new auf --> INSERT
			model.put("postAction", "/stakeholder/new");
			model.put("stakeholderDetail", new Stakeholder());

		} else {
			log.trace("GET /stakeholder für UPDATE mit staId " + staIdString);
			// der Submit-Button ruft /stakeholder/update auf --> UPDATE
			model.put("postAction", "/stakeholder/update");

			// damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt
			// werden wird es geladen und dann
			// dem Modell unter dem Namen "stakeholderDetail" hinzugefügt. stakeholderDetail muss
			// dem im HTML-Template verwendeten Namen entsprechen
			Long staId = Long.parseLong(staIdString);
			Stakeholder i = stakeholderRepo.getByStaId(staId);
			model.put("stakeholderDetail", i);
		}

		// das Template stakeholderDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "stakeholderDetailTemplate");
	}

}
