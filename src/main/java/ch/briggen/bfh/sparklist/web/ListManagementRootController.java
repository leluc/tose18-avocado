package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import ch.briggen.bfh.sparklist.domain.Department;
import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import ch.briggen.bfh.sparklist.domain.ProjectTeam;
import ch.briggen.bfh.sparklist.domain.ProjectTeamRepository;
import ch.briggen.bfh.sparklist.domain.Corporation;
import ch.briggen.bfh.sparklist.domain.CorporationRepository;
import ch.briggen.bfh.sparklist.domain.Location;
import ch.briggen.bfh.sparklist.domain.LocationRepository;
import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import ch.briggen.bfh.sparklist.domain.Report;
import ch.briggen.bfh.sparklist.domain.ReportRepository;
import ch.briggen.bfh.sparklist.domain.ProjectManagement;
import ch.briggen.bfh.sparklist.domain.ProjectManagementRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller Liefert unter "/" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung
 * !!!
 * 
 * @author M. Briggen
 *
 */
public class ListManagementRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementRootController.class);
	
	/*
	 * Hier müsst ihr die Repositories erstellen, nicht vergessen die Klassen oben unter "import" in diese File zu importieren
	 */
	ItemRepository repositoryItem = new ItemRepository();
	EmployeeRepository repositoryEmployee = new EmployeeRepository();
	ProjectRepository repositoryProject = new ProjectRepository();
	StakeholderRepository repositoryStakeholder = new StakeholderRepository();
	DepartmentRepository repositoryDepartment = new DepartmentRepository();
	ProjectTeamRepository repositoryProjectTeam = new ProjectTeamRepository();
	CorporationRepository repositoryCorporation = new CorporationRepository();
	LocationRepository repositoryLocation = new LocationRepository();
	BudgetRepository repositoryBudget = new BudgetRepository();
	CostRepository repositoryCost = new CostRepository();;	
	ReportRepository repositoryReport = new ReportRepository();	
	ProjectManagementRepository repositoryProjectManagement = new ProjectManagementRepository();
	

	/**
	 * Liefert die Liste als Root-Seite "/" zurück
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		/**
		 * Items werden geladen und die Collection dann für das Template unter dem namen
		 * "list" bereitgestellt // Das Template muss dann auch den Namen "list"
		 * verwenden.
		 */
		
		/*
		 * Hier müsst ihr immer die Repositories reinladen mittels folgendem Schema
		 * 
		 * model.put("list<<KLASSE>>", repository<<KLASSE>>.getAll());
		 */
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("listItem", repositoryItem.getAll());
		model.put("listEmployee", repositoryEmployee.getAll());
		model.put("listProject", repositoryProject.getAll());
		model.put("listStakeholder", repositoryStakeholder.getAll());
		model.put("listDepartment", repositoryDepartment.getAll());
		model.put("listProjectTeam", repositoryProjectTeam.getAll());
		model.put("listCorporation", repositoryCorporation.getAll());
		model.put("listBudget", repositoryBudget.getAll());
		model.put("listCost", repositoryCost.getAll());
		model.put("listReport", repositoryReport.getAll());
		model.put("listProjectManagement", repositoryProjectManagement.getAll());
		return new ModelAndView(model, "listTemplate");
	}
}
