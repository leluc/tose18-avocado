package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ReportRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Report
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ReportDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ReportDeleteController.class);
		
	private ReportRepository reportRepo = new ReportRepository();
	

	/**
	 * Löscht das Report mit der übergebenen id in der Datenbank
	 * /report/delete&id=987 löscht das Report mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /report/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String repId = request.queryParams("repId");
		log.trace("GET /report/delete mit repId " + repId);
		
		Long longRepId = Long.parseLong(repId);
		reportRepo.delete(longRepId);
		response.redirect("/");
		return null;
	}
}


