package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Employee;
import ch.briggen.bfh.sparklist.domain.EmployeeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class EmployeeNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeNewController.class);
		
	private EmployeeRepository employeeRepo = new EmployeeRepository();
	
	/**
	 * Erstellt ein neues Employee in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /employee&empId=99  wenn die empId 99 war.)
	 * 
	 * Hört auf POST /employee/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Employee employeeDetail = EmployeeWebHelper.employeeFromWeb(request);
		log.trace("POST /employee/new mit employeeDetail " + employeeDetail);
		
		//insert gibt die von der DB erstellte empId zurück.
		long empId = employeeRepo.insert(employeeDetail);
		
		//die neue empId wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /employee?empId=432932
		response.redirect("/employee/list");
		return null;
	}
}


