package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import ch.briggen.bfh.sparklist.domain.BudgetRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Budgets
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class BudgetNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(BudgetNewController.class);
		
	private BudgetRepository budgetRepo = new BudgetRepository();
	
	/**
	 * Erstellt ein neues Budget in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /budget&depId=99  wenn die budId 99 war.)
	 * 
	 * Hört auf POST /budget/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Budget budgetDetail = BudgetWebHelper.budgetFromWeb(request);
		log.trace("POST /budget/new mit budgetDetail " + budgetDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = budgetRepo.insert(budgetDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /budget?depId=432932
		response.redirect("/budget/list");
		return null;
	}
}


