package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Budget;
import spark.Request;

class BudgetWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(BudgetWebHelper.class);
	
	public static Budget budgetFromWeb(Request request)
	{
		return new Budget(
				Long.parseLong(request.queryParams("budgetDetail.budId")),
				Long.parseLong(request.queryParams("budgetDetail.budAmount")),
				Long.parseLong(request.queryParams("budgetDetail.staIdFk")),
				Long.parseLong(request.queryParams("budgetDetail.proIdFk"))
				);
	}
	}