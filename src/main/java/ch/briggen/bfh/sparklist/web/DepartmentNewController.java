package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Department;
import ch.briggen.bfh.sparklist.domain.DepartmentRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Employees
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class DepartmentNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(DepartmentNewController.class);
		
	private DepartmentRepository departmentRepo = new DepartmentRepository();
	
	/**
	 * Erstellt ein neues Department in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /department&depId=99  wenn die depId 99 war.)
	 * 
	 * Hört auf POST /department/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Department departmentDetail = DepartmentWebHelper.departmentFromWeb(request);
		log.trace("POST /department/new mit departmentDetail " + departmentDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long depId = departmentRepo.insert(departmentDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /department?depId=432932
		response.redirect("/department/list");
		return null;
	}
}


