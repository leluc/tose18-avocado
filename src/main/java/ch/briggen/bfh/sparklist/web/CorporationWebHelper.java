package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Corporation;
import spark.Request;

class CorporationWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(CorporationWebHelper.class);
	
	public static Corporation corporationFromWeb(Request request)
	{
		return new Corporation(
				Long.parseLong(request.queryParams("corporationDetail.corpId")),
				Integer.parseInt(request.queryParams("corporationDetail.corpNumber")),
				request.queryParams("corporationDetail.corpName"),
				request.queryParams("corporationDetail.streetName"),
				Integer.parseInt(request.queryParams("corporationDetail.streetNumber")),
				Long.parseLong(request.queryParams("corporationDetail.locIdFk")));
	}

}