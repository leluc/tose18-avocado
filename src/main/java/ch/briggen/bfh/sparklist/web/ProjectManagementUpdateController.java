package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectManagement;
import ch.briggen.bfh.sparklist.domain.ProjectManagementRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen ProjectManagements
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectManagementUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ProjectManagementUpdateController.class);
		
	private ProjectManagementRepository projectManagementRepo = new ProjectManagementRepository();
	


	/**
	 * Schreibt das geänderte ProjectManagement zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/projectManagement) mit der ProjectManagement-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /projectManagement/update
	 * 
	 * @return redirect nach /projectManagement: via Browser wird /projectManagement aufgerufen, also editProjectManagement weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		ProjectManagement projectManagementDetail = ProjectManagementWebHelper.projectManagementFromWeb(request);
		
		log.trace("POST /projectManagement/update mit projectManagementDetail " + projectManagementDetail);
		
		//Speichern des ProjectManagements in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /projectManagement&manId=3 (wenn budgetDetail.getManId == 3 war)
		projectManagementRepo.save(projectManagementDetail);
		response.redirect("/projectManagement?empId="+ projectManagementDetail.getManId());
		return null;
	}
}


