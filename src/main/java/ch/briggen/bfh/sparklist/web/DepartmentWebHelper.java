package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Department;
import spark.Request;

class DepartmentWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(DepartmentWebHelper.class);
	
	public static Department departmentFromWeb(Request request)
	{
		return new Department(
				Long.parseLong(request.queryParams("departmentDetail.depId")),
				request.queryParams("departmentDetail.depName"));
	}
	}