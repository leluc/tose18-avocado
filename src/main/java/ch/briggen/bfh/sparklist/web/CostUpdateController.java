package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Cost;
import ch.briggen.bfh.sparklist.domain.CostRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Costs
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class CostUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(CostUpdateController.class);
		
	private CostRepository costRepo = new CostRepository();
	


	/**
	 * Schreibt das geänderte Cost zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/cost) mit der Cost-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /cost/update
	 * 
	 * @return redirect nach /cost: via Browser wird /cost aufgerufen, also editCost weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Cost costDetail = CostWebHelper.costFromWeb(request);
		
		log.trace("POST /cost/update mit costDetail " + costDetail);
		
		//Speichern des costs in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /budget&depId=3 (wenn costDetail.getcostId == 3 war)
		costRepo.save(costDetail);
		response.redirect("/cost?empId="+costDetail.getCostId());
		return null;
	}
}


