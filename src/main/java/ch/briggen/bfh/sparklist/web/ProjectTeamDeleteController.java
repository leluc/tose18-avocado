package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectTeamRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Department
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectTeamDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectTeamDeleteController.class);
		
	private ProjectTeamRepository projectTeamRepo = new ProjectTeamRepository();
	

	/**
	 * Löscht das Department mit der übergebenen id in der Datenbank
	 * /employee/delete&id=987 löscht das Department mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /department/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String teamIdString = request.queryParams("teamId");
		Long teamId = Long.parseLong(teamIdString);
		
		projectTeamRepo.delete(teamId);
		response.redirect("/projectTeam/list");
		return null;
	}
}


