package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.LocationRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Location
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class LocationDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(LocationDeleteController.class);
		
	private LocationRepository locationRepo = new LocationRepository();
	

	/**
	 * Löscht das Corporation mit der übergebenen id in der Datenbank
	 * /employee/delete&id=987 löscht das Corporation mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /corporation/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String locId = request.queryParams("locId");
		log.trace("GET /location/delete mit locpId " + locId);
		
		Long longLocId = Long.parseLong(locId);
		locationRepo.delete(longLocId);
		response.redirect("/");
		return null;
	}
}


