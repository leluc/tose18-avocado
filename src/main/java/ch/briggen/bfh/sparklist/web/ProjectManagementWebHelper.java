package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectManagement;
import spark.Request;

class ProjectManagementWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjectManagementWebHelper.class);
	
	public static ProjectManagement projectManagementFromWeb(Request request)
	{
		return new ProjectManagement(
				Long.parseLong(request.queryParams("projectManagementDetail.manId")),
				Long.parseLong(request.queryParams("projectManagementDetail.manPriority")),
				Long.parseLong(request.queryParams("projectManagementDetail.repIdFk")),
				Long.parseLong(request.queryParams("projectManagementDetail.budIdFk")),
				Long.parseLong(request.queryParams("projectManagementDetail.teamIdFk"))
				);
	}
	}