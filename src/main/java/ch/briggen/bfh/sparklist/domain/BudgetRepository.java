package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Budgets. 
 * Hier werden alle Funktionen für die DB-Operationen zu Budgets implementiert
 * @author Marcel Briggen
 *
 */


public class BudgetRepository {
	
	private final Logger log = LoggerFactory.getLogger(BudgetRepository.class);
	

	/**
	 * Liefert alle Budgets in der Datenbank
	 * @return Collection aller Budgets
	 */
	public Collection<Budget> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select budId, budAmount, staIdFk, proIdFk from budgets");
			ResultSet rs = stmt.executeQuery();
			return mapBudgets(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all budgets. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Budgets mit dem angegebenen Amount
	 * @param name
	 * @return Collection mit dem Amount "amount"
	 */
	public Collection<Budget> getByBudAmount(long budAmount) {
		log.trace("getByBudAmount " + budAmount);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select budId, staIdFk, proIdFk from budgets where budAmount=?");
			stmt.setLong(1, budAmount);
			ResultSet rs = stmt.executeQuery();
			return mapBudgets(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving budgets by budAmount " + budAmount;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Project mit der übergebenen Id
	 * @param id id des Project
	 * @return Project oder NULL
	 */
	public Budget getByBudId(long budId) {
		log.trace("getByBudId " + budId);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from budgets where budId=?");
			stmt.setLong(1, budId);
			ResultSet rs = stmt.executeQuery();
			return mapBudgets(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving budgets by budId " + budId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Budget in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Budget i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update budgets set budAmount=?, staIdFk=?, proIdFk=? where budId=?");
			stmt.setLong(1, i.getBudAmount());
			stmt.setLong(2, i.getStaIdFk());
			stmt.setLong(3, i.getProIdFk());
			stmt.setLong(4, i.getBudId());			
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating budgets " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Budget mit der angegebenen Id von der DB
	 * @param id Budget ID
	 */
	public void delete(long budId) {
		log.trace("delete " + budId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from budgets where budId=?");
			stmt.setLong(1, budId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing budgets by budId " + budId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Budget in der DB. INSERT.
	 * @param i neu zu erstellendes Budget
	 * @return Liefert die von der DB generierte id des neuen Budgets zurück
	 */
	public long insert(Budget i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into budgets (budAmount, staIdFk, proIdFk) values (?,?,?)");
			stmt.setLong(1, i.getBudAmount());
			stmt.setLong(2, i.getStaIdFk());
			stmt.setLong(3, i.getProIdFk());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long budId = key.getLong(1);
			return budId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating budgets " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Budget-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Budget> mapBudgets(ResultSet rs) throws SQLException 
	{
		LinkedList<Budget> list = new LinkedList<Budget>();
		while(rs.next())
		{
			Budget i = new Budget(
					rs.getLong("budId"),
					rs.getLong("budAmount"),
					rs.getLong("staIdFk"),
					rs.getLong("proIdFk"));
			
			list.add(i);
		}
		return list;
	}

}
