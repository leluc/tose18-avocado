package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projects. 
 * Hier werden alle Funktionen für die DB-Operationen zu Projects implementiert
 * @author Marcel Briggen
 *
 */


public class ProjectRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectRepository.class);
	

	/**
	 * Liefert alle projects in der Datenbank
	 * @return Collection aller Projects
	 */
	public Collection<Project> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select proId, projectName, description, depIdFk, empIdFk from projects");
			ResultSet rs = stmt.executeQuery();
			return mapProjects(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projects. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Projects mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Project> getByProjectname(long projectname) {
		log.trace("getByProjectname " + projectname);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select proId, projectName, description, depIdFk, empIdFk from projects where projectname=?");
			stmt.setLong(1, projectname);
			ResultSet rs = stmt.executeQuery();
			return mapProjects(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by projectname " + projectname;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Project mit der übergebenen Id
	 * @param id id des Project
	 * @return Project oder NULL
	 */
	public Project getByProId(long proId) {
		log.trace("getByProId " + proId);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select proId, projectname, description, depIdFk, empIdFk  from projects where proId=?");
			stmt.setLong(1, proId);
			ResultSet rs = stmt.executeQuery();
			return mapProjects(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by proId " + proId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene project in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Project i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projects set projectname=?, description=?, depIdFk=?, empIdFk=? where proId=?");
			stmt.setString(1, i.getProjectname());
			stmt.setString(2, i.getDescription());
			stmt.setLong(3, i.getDepIdFk());
			stmt.setLong(4, i.getEmpIdFk());
			stmt.setLong(5, i.getProId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projects " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Project mit der angegebenen Id von der DB
	 * @param id Project ID
	 */
	public void delete(long proId) {
		log.trace("delete " + proId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projects where proId=?");
			stmt.setLong(1, proId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projects by proId " + proId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Project in der DB. INSERT.
	 * @param i neu zu erstellendes Project
	 * @return Liefert die von der DB generierte id des neuen Projects zurück
	 */
	public long insert(Project i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projects (projectName, description, depIdFk, empIdFk) values (?,?,?,?)");
			stmt.setString(1, i.getProjectname());
			stmt.setString(2, i.getDescription());
			stmt.setLong(3, i.getDepIdFk());
			stmt.setLong(4, i.getEmpIdFk());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long proId = key.getLong(1);
			return proId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projects " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Project-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Project> mapProjects(ResultSet rs) throws SQLException 
	{
		LinkedList<Project> list = new LinkedList<Project>();
		while(rs.next())
		{
			Project i = new Project(
					rs.getLong("proId"),
					rs.getString("projectname"),
					rs.getString("description"),
					rs.getLong("depIdFk"),
					rs.getLong("empIdFk"));
			list.add(i);
		}
		return list;
	}

}
