package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjectTeamRepository {

	private final Logger log = LoggerFactory.getLogger(ProjectTeamRepository.class);

	/**
	 * Liefert alle projectteams in der Datenbank
	 * 
	 * @return Collection aller projectteams
	 */
	public Collection<ProjectTeam> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select teamId, proIdFk, empIdFk from projectTeams");
			ResultSet rs = stmt.executeQuery();
			return mapProjectTeams(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all projectTeams. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert das Report mit der übergebenen Id
	 * 
	 * @param id
	 *            id des Report
	 * @return Report oder NULL
	 */
	public ProjectTeam getByTeamId(long teamId) {
		log.trace("getByTeamId " + teamId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select empIdFk, proIdFk, teamid from projectTeams where teamId=?");
			stmt.setLong(1, teamId);
			ResultSet rs = stmt.executeQuery();
			return mapProjectTeams(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving projectTeams by teamId " + teamId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}
	

	/**
	 * Speichert das übergebene report in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(ProjectTeam i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update projectTeams set proIdFk=?, empIdFk=? where teamId=?");
			stmt.setLong(1, i.getProIdFk());
			stmt.setLong(2, i.getEmpIdFk());
			stmt.setLong(3, i.getTeamId());			
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating projectTeams " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht das Report mit der angegebenen Id von der DB
	 * 
	 * @param id
	 *            Report ID
	 */
	public void delete(long teamId) {
		log.trace("delete " + teamId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from projectTeams where teamId=?");
			stmt.setLong(1, teamId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing projectTeams by teamId + teamId";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das angegebene Report in der DB. INSERT.
	 * 
	 * @param i
	 *            neu zu erstellendes Report
	 * @return Liefert die von der DB generierte id des neuen Reports zurück
	 */
	public long insert(ProjectTeam i) {

		log.trace("insert " + i);

		// Integer id = jdbc.queryForObject("select IDENTITY();",
		// Integer.class);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into projectTeams (proIdFk, empIdFk) values (?,?)");
			stmt.setLong(1, i.getProIdFk());
			stmt.setLong(2, i.getEmpIdFk());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long proId = key.getLong(1);
			return proId;
		} catch (SQLException e) {
			String msg = "SQL error while updating projectTeams " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper zum konvertieren der Resultsets in Report-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @author Marcel Briggen
	 * @throws SQLException
	 *
	 */
	private static Collection<ProjectTeam> mapProjectTeams(ResultSet rs) throws SQLException {
		LinkedList<ProjectTeam> list = new LinkedList<ProjectTeam>();
		while (rs.next()) {
			ProjectTeam i = new ProjectTeam(
					rs.getLong("teamId"),  					
					rs.getLong("proIdFk"),  
					rs.getLong("empIdFk")
					);
			list.add(i);
		}
		return list;
	}

}