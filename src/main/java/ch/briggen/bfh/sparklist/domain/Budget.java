package ch.briggen.bfh.sparklist.domain;

/**
 * 
 * @author Marcel Briggen
 *
 */
public class Budget {

	private long budId;
	private long budAmount;
	private long staIdFk;
	private long proIdFk;
	
	public Budget() {

	}

	public Budget(long budId, long budAmount, long staIdFk, long proIdFk) {
		this.budId = budId;
		this.budAmount = budAmount;
		this.staIdFk = staIdFk;
		this.proIdFk = proIdFk;
	}

	public long getBudId() {
		return budId;
	}

	public void setBudId(long budId) {
		this.budId = budId;
	}

	public long getBudAmount() {
		return budAmount;
	}

	public void setBudAmount(long budAmount) {
		this.budAmount = budAmount;
	}
	
	public long getStaIdFk() {
		return staIdFk;
	}

	public void setStaIdFk(long staIdFk) {
		this.staIdFk = staIdFk;
	}
	
	public long getProIdFk() {
		return proIdFk;
	}

	public void setProIdFk(long proIdFk) {
		this.proIdFk = proIdFk;
	}
	


	@Override
	public String toString() {
		return String.format("Budget:{budId: %d; budAmount: %d; staIdFk: %d; proIdFk: %d;}",
				budId, budAmount, staIdFk, proIdFk);
	}

}
