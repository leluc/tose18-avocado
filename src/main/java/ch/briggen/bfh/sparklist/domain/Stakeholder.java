package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name und Beschreibung (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Stakeholder {

	private long staId;
	private String staName;
	private String staDescription;
	private String comment;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Stakeholder() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param staId
	 *            Eindeutige Id
	 * @param staName
	 *            Name des eintrags in der Liste
	 * @param staDescription
	 *            Beschreibung
	 *    @param comment
	 *            Kommentar                  
	 */
	public Stakeholder(long staId, String staName, String staDescription, String comment) {
		this.staId = staId;
		this.staName = staName;
		this.staDescription = staDescription;
		this.comment = comment;
	}

	public String getStaName() {
		return staName;
	}

	public void setStaName(String staName) {
		this.staName = staName;
	}

	public String getStaDescription() {
		return staDescription;
	}

	public void setStaDescription(String staDescription) {
		this.staDescription = staDescription;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
		
	}
	public long getStaId() {
		return staId;
	}

	public void setStaId(long staId) {
		this.staId = staId;
	}
	
	
	@Override
	public String toString() {
		return String.format("Stakeholder:{staId: %d; staName: %s; staDescription: %s; comment: %s}", staId, staName, staDescription, comment);
	}

}
