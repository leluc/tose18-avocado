package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Corporation. Hier werden alle Funktionen für die
 * DB-Operationen zu Corporation implementiert
 * 
 * @author Marcel Briggen
 *
 */

public class CorporationRepository {

	private final Logger log = LoggerFactory.getLogger(CorporationRepository.class);

	/**
	 * Liefert alle Corporation in der Datenbank
	 * 
	 * @return Collection aller Corporation
	 */
	public Collection<Corporation> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select corpId, corpNumber, corpName, streetName, streetNumber, locIdFk from corporations");
			ResultSet rs = stmt.executeQuery();
			return mapCorporations(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all corporations. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Corporation mit dem angegebenen Namen
	 * 
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Corporation> getByCorpName(String corpName) {
		log.trace("getByCorpName " + corpName);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select corpId, corpNumber, streetName, streetNumber, locIdFk where corpName=?");
			stmt.setString(1, corpName);
			ResultSet rs = stmt.executeQuery();
			return mapCorporations(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving corporations by corpName " + corpName;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert das Corporation mit der übergebenen Id
	 * 
	 * @param id
	 *            id des Corporation
	 * @return Employee oder NULL
	 */
	public Corporation getByCorpId(long corpId) {
		log.trace("getByCorpId " + corpId);

		// TODO: There is an issue with this repository method. Find and fix it!
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select corpNumber, corpName, streetName, streetNumber, locIdFk where corpId=?");
			stmt.setLong(1, corpId);
			ResultSet rs = stmt.executeQuery();
			return mapCorporations(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving corporations by corpId " + corpId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das übergebene Corporation in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(Corporation i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update corporations set corpNumber, corpName, streetName, streetNumber, locIdFk where corpId=?");
			stmt.setInt(1, i.getCorpNumber());
			stmt.setString(2, i.getCorpName());
			stmt.setString(3, i.getStreetName());
			stmt.setInt(4, i.getStreetNumber());
			stmt.setLong(5, i.getLocIdFk());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating corporation " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Löscht das Corporation mit der angegebenen Id von der DB
	 * 
	 * @param id
	 *            Corporation ID
	 */
	public void delete(long corpId) {
		log.trace("delete " + corpId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from corporations where corpId=?");
			stmt.setLong(1, corpId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing corporations by corpId " + corpId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das angegebene Corporation in der DB. INSERT.
	 * 
	 * @param i
	 *            neu zu erstellendes Corporation
	 * @return Liefert die von der DB generierte id des neuen Corporation zurück
	 */
	public long insert(Corporation i) {

		log.trace("insert " + i);

		// Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into corporations (corpId, corpNumber, corpName, streetName, streetNumber, locIdFk) values (?,?,?,?,?,?)");
			stmt.setInt(1, i.getCorpNumber());
			stmt.setString(2, i.getCorpName());
			stmt.setString(3, i.getStreetName());
			stmt.setInt(4, i.getStreetNumber());
			stmt.setLong(5, i.getLocIdFk());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long corpId = key.getLong(1);
			return corpId;
		} catch (SQLException e) {
			String msg = "SQL error while updating corporations " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}
	


	/**
	 * Helper zum konvertieren der Resultsets in Corporation-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @author Marcel Briggen
	 * @throws SQLException
	 *
	 */
	private static Collection<Corporation> mapCorporations(ResultSet rs) throws SQLException{
		LinkedList<Corporation> list = new LinkedList<Corporation>();
		while (rs.next()) {
			Corporation i = new Corporation(
					rs.getLong("corpId"), 
					rs.getInt("corpNumber"), 
					rs.getString("corpName"),
					rs.getString("streetName"),
					rs.getInt("streetNumber"),
					rs.getLong("locIdFk"));
			list.add(i);
		}
		return list;
	}

}
