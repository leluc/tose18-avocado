package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Projekt-ID, Projektname, Projektmanager und Beschreibung (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Department {

	private long depId;
	private String depName;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Department() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige depId
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public Department(long depId, String depName) {
		this.depId = depId;
		this.depName = depName;
	}

	public long getDepId() {
		return depId;
	}

	public void setDepId(long depId) {
		this.depId = depId;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}


	@Override
	public String toString() {
		return String.format("Department:{depId: %d; depName: %s;}",
				depId, depName);
	}

}
