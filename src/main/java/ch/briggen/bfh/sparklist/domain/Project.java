package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Projekt-ID, Projektname, Projektmanager und Beschreibung (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Project {

	private long proId;
	private String projectname;
	private String description;
	private long depIdFk;
	private long empIdFk;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Project() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige Id
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public Project(long proId, String projectname, String description, long depIdFk, long empIdFk) {
		this.proId = proId;
		this.projectname = projectname;
		this.description = description;
		this.depIdFk = depIdFk;
		this.empIdFk = empIdFk;
	}

	public long getProId() {
		return proId;
	}

	public void setProId(long proId) {
		this.proId = proId;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getDepIdFk() {
		return depIdFk;
	}

	public void setDepIdFk(long depIdFk) {
		this.depIdFk = depIdFk;
	}
	
	public long getEmpIdFk() {
		return empIdFk;
	}

	public void setEmpIdFk(long empIdFk) {
		this.depIdFk = empIdFk;
	}

	@Override
	public String toString() {
		return "Project [proId=" + proId + ", projectname=" + projectname + ", description=" + description
				+ ", depIdFk=" + depIdFk + ", empIdFk=" + empIdFk + "]";
	}
}
