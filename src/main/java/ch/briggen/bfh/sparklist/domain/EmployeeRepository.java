package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Employees. Hier werden alle Funktionen für die
 * DB-Operationen zu Employees implementiert
 * 
 * @author Marcel Briggen
 *
 */

public class EmployeeRepository {

	private final Logger log = LoggerFactory.getLogger(EmployeeRepository.class);

	/**
	 * Liefert alle employees in der Datenbank
	 * 
	 * @return Collection aller Employees
	 */
	public Collection<Employee> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select empId, prename, lastname, empRole, depIdFk from employees");
			ResultSet rs = stmt.executeQuery();
			return mapEmployees(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all employees. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Employees mit dem angegebenen Namen
	 * 
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Employee> getByLastname(String lastname) {
		log.trace("getByLastname " + lastname);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select empId, prename, lastname, empRole, depIdFk from employees where lastname=?");
			stmt.setString(1, lastname);
			ResultSet rs = stmt.executeQuery();
			return mapEmployees(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving employees by lastname " + lastname;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert das Employee mit der übergebenen Id
	 * 
	 * @param id
	 *            id des Employee
	 * @return Employee oder NULL
	 */
	public Employee getByEmpId(long empId) {
		log.trace("getByempId " + empId);

		// TODO: There is an issue with this repository method. Find and fix it!
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select empId, prename, lastname, empRole, depIdFk from employees where empId=?");
			stmt.setLong(1, empId);
			ResultSet rs = stmt.executeQuery();
			return mapEmployees(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving employees by empId " + empId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das übergebene employee in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(Employee i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update employees set prename=?, lastname=?, empRole=?, depIdFk=? where empId=?");
			stmt.setString(1, i.getPrename());
			stmt.setString(2, i.getLastname());
			stmt.setString(3, i.getEmpRole());
			stmt.setLong(4, i.getDepIdFk());
			stmt.setLong(5, i.getEmpId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating employees " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht das Employee mit der angegebenen Id von der DB
	 * 
	 * @param id
	 *            Employee ID
	 */
	public void delete(long empId) {
		log.trace("delete " + empId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from employees where empId=?");
			stmt.setLong(1, empId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing employees by empId " + empId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das angegebene Employee in der DB. INSERT.
	 * 
	 * @param i
	 *            neu zu erstellendes Employee
	 * @return Liefert die von der DB generierte id des neuen Employees zurück
	 */
	public long insert(Employee i) {

		log.trace("insert " + i);

		// Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into employees (prename, lastname, empRole, depIdFk) values (?,?,?,?)");
			stmt.setString(1, i.getPrename());
			stmt.setString(2, i.getLastname());
			stmt.setString(3, i.getEmpRole());
			stmt.setLong(4, i.getDepIdFk());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while updating employees " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper zum konvertieren der Resultsets in Employee-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @author Marcel Briggen
	 * @throws SQLException
	 *
	 */
	private static Collection<Employee> mapEmployees(ResultSet rs) throws SQLException{
		LinkedList<Employee> list = new LinkedList<Employee>();
		while (rs.next()) {
			Employee i = new Employee(
					rs.getLong("empId"), 
					rs.getString("prename"), 
					rs.getString("lastname"),
					rs.getString("empRole"),
					rs.getLong("depIdFk"));
			list.add(i);
		}
		return list;
	}

}
