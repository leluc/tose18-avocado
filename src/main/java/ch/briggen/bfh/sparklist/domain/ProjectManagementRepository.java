package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Budgets. 
 * Hier werden alle Funktionen für die DB-Operationen zu Budgets implementiert
 * @author Marcel Briggen
 *
 */


public class ProjectManagementRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectManagementRepository.class);
	

	/**
	 * Liefert alle ProjectManagements in der Datenbank
	 * @return Collection aller ProjectManagements
	 */
	public Collection<ProjectManagement> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select manId, manPriority, repIdFk, budIdFk, teamIdFk from projectManagements");
			ResultSet rs = stmt.executeQuery();
			return mapProjectManagements(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projectManagements. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle ProjectManagements mit dem angegebenen Amount
	 * @param name
	 * @return Collection mit der Priority "priority"
	 */
	public Collection<ProjectManagement> getByManPriority(long manPriority) {
		log.trace("getByManPriority " + manPriority);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select manId, manPriority, repIdFk, budIdFk, teamIdFk from projectManagements where manPriority=?");
			stmt.setLong(1, manPriority);
			ResultSet rs = stmt.executeQuery();
			return mapProjectManagements(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projectManagements by manPriority " + manPriority;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das ProjectManagement mit der übergebenen Id
	 * @param id id des Project
	 * @return Project oder NULL
	 */
	public ProjectManagement getByManId(long manId) {
		log.trace("getByManId " + manId);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select manId, manPriority, repIdFk, budIdFk, teamIdFk from projectManagements where manId=?");
			stmt.setLong(1, manId);
			ResultSet rs = stmt.executeQuery();
			return mapProjectManagements(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projectManagements by manId " + manId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene ProjectManagement in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(ProjectManagement i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projectManagements set manId, manPriority, repIdFk, budIdFk, teamIdFk from projectManagements where manId=?");
			stmt.setLong(1, i.getManId());
			stmt.setLong(2, i.getManPriority());
			stmt.setLong(3, i.getRepIdFk());
			stmt.setLong(4, i.getBudIdFk());
			stmt.setLong(5, i.getTeamIdFk());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projectManagements " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das ProjectManagement mit der angegebenen Id von der DB
	 * @param id ProjectManagement ID
	 */
	public void delete(long manId) {
		log.trace("delete " + manId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from ProjectManagements where manId=?");
			stmt.setLong(1, manId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projectManagements by manId " + manId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene ProjectManagement in der DB. INSERT.
	 * @param i neu zu erstellendes ProjectManagement
	 * @return Liefert die von der DB generierte id des neuen ProjectManagements zurück
	 */
	public long insert(ProjectManagement i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projectManagements (manId, manPriority, repIdFk, budIdFk, teamIdFk) values (?,?,?,?,?)");
			stmt.setLong(1, i.getManId());
			stmt.setLong(2, i.getManPriority());
			stmt.setLong(3, i.getRepIdFk());
			stmt.setLong(4, i.getBudIdFk());
			stmt.setLong(5, i.getTeamIdFk());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long budId = key.getLong(1);
			return budId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projectManagements " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in ProjectManagement-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<ProjectManagement> mapProjectManagements(ResultSet rs) throws SQLException 
	{
		LinkedList<ProjectManagement> list = new LinkedList<ProjectManagement>();
		while(rs.next())
		{
			ProjectManagement i = new ProjectManagement(
					rs.getLong("manId"),
					rs.getLong("manPriority"),
					rs.getLong("repIdFk"),
					rs.getLong("budIdFk"),
					rs.getLong("teamIdFk"));
			list.add(i);
		}
		return list;
	}

}