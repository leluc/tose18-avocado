package ch.briggen.bfh.sparklist.domain;

/**
 * 
 * @author Marcel Briggen
 *
 */
public class Cost {

	private long costId;
	private long budIdFk;
	private long corpIdFk;
	private int amount;
	private String fallingdue; 
	

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Cost() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige Id
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public Cost(long costId, long budIdFk, long corpIdFk, int amount, String fallingdue) {
		this.costId = costId;
		this.budIdFk = budIdFk;
		this.corpIdFk = corpIdFk;
		this.amount = amount;
		this.fallingdue = fallingdue;
	}

	public long getCostId() {
		return costId;
	}

	public void setCostId(long costId) {
		this.costId = costId;
	}

	public long getBudIdFk() {
		return budIdFk;
	}

	public void setBudIdFk(long budIdFk) {
		this.budIdFk = budIdFk;
	}
	
	public long getCorpIdFk() {
		return corpIdFk;
	}

	public void setCorpIdFk(long corpIdFk) {
		this.corpIdFk = corpIdFk;
	}
	
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public String getFallingdue() {
		return fallingdue;
	}

	public void setFallingdue(String fallingdue) {
		this.fallingdue = fallingdue;
	}
	


	@Override
	public String toString() {
		return String.format("Cost:{costId: %d; budIdFk: %d; corpIdFk: %d; amount: %d; fallingdue; %s}",
				costId, budIdFk, corpIdFk, amount, fallingdue);
	}

}
