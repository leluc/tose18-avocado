package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projects. 
 * Hier werden alle Funktionen für die DB-Operationen zu Projects implementiert
 * @author Marcel Briggen
 *
 */


public class ReportRepository {
	
	private final Logger log = LoggerFactory.getLogger(ReportRepository.class);
	

	/**
	 * Liefert alle departments in der Datenbank
	 * @return Collection aller Departments
	 */
	public Collection<Report> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select repId, proIdFk, corpIdFk, repGoal, repDescription, budIdFk, teamIdFk, staIdFk from reports");
			ResultSet rs = stmt.executeQuery();
			return mapReports(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all reports. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Reports mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Report> getByRepName(long repName) {
		log.trace("getByRepName " + repName);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select repId, repName, proIdFk, corpIdFk, repGoal, repDescription, budIdFk, teamIdFk, staIdFk from reports where repName=?");
			stmt.setLong(1, repName);
			ResultSet rs = stmt.executeQuery();
			return mapReports(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving reports by repName " + repName;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Report mit der übergebenen Id
	 * @param id id des Report
	 * @return Project oder NULL
	 */
	public Report getByRepId(long repId) {
		log.trace("getByRepId " + repId);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select repId, repName, proIdFk, corpIdFk, repGoal, repDescription, budIdFk, teamIdFk, staIdFk from reports where repId=?");
			stmt.setLong(1, repId);
			ResultSet rs = stmt.executeQuery();
			return mapReports(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving reports by repId " + repId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Report in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Report i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update reports set repId=?, repName=?, proIdFk=?, corpIdFk=?, repGoal=?, repDescription=?, budIdFk=?, teamIdFk=?, staIdFk=?");
			stmt.setLong(1, i.getRepId());
			stmt.setString(2, i.getRepName());
			stmt.setLong(3, i.getProIdFk());
			stmt.setLong(4, i.getCorpIdFk());
			stmt.setString(5, i.getRepGoal());
			stmt.setString(6, i.getRepDescription());
			stmt.setLong(7, i.getBudIdFk());
			stmt.setLong(8, i.getTeamIdFk());
			stmt.setLong(9, i.getStaIdFk());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating reports " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Report mit der angegebenen Id von der DB
	 * @param id Report ID
	 */
	public void delete(long repId) {
		log.trace("delete " + repId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from reports where id=?");
			stmt.setLong(1, repId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing reports by repId " + repId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Report in der DB. INSERT.
	 * @param i neu zu erstellendes Report
	 * @return Liefert die von der DB generierte id des neuen Reports zurück
	 */
	public long insert(Report i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into reports (repId, repName, proIdFk, corpIdFk, repGoal, repDescription, budIdFk, teamIdFk, staIdFk) values (?,?,?,?,?,?,?,?,?)");
			stmt.setLong(1, i.getRepId());
			stmt.setString(2, i.getRepName());
			stmt.setLong(3, i.getProIdFk());
			stmt.setLong(4, i.getCorpIdFk());
			stmt.setString(5, i.getRepGoal());
			stmt.setString(6, i.getRepDescription());
			stmt.setLong(7, i.getBudIdFk());
			stmt.setLong(8, i.getTeamIdFk());
			stmt.setLong(9, i.getStaIdFk());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating reports " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in report-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Report> mapReports(ResultSet rs) throws SQLException 
	{
		LinkedList<Report> list = new LinkedList<Report>();
		while(rs.next())
		{
			Report i = new Report(
					rs.getLong("repId"),
					rs.getString("repName"),
					rs.getLong("proIdFk"),
					rs.getLong("corpIdFk"),
					rs.getString("repGoal"),
					rs.getString("repDescription"),
					rs.getLong("budIdFk"),
					rs.getLong("teamIdFk"),
					rs.getLong("staIdFk"));
			
			list.add(i);
		}
		return list;
	}

}
