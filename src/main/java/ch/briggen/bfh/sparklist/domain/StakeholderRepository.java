package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Stakeholder. Hier werden alle Funktionen für die
 * DB-Operationen zu Stakeholder implementiert
 * 
 * @author Marcel Briggen
 *
 */

public class StakeholderRepository {

	private final Logger log = LoggerFactory.getLogger(StakeholderRepository.class);

	/**
	 * Liefert alle stakeholder in der Datenbank
	 * 
	 * @return Collection aller Stakeholder
	 */
	public Collection<Stakeholder> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select staId, staName, staDescription, comment from stakeholders");
			ResultSet rs = stmt.executeQuery();
			return mapStakeholders(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all stakeholders. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Stakeholders mit dem angegebenen Namen
	 * 
	 * @param staName
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Stakeholder> getByStaName(String staName) {
		log.trace("getByStaName " + staName);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select staId, staName, staDescription, comment from stakeholders where staName=?");
			stmt.setString(1, staName);
			ResultSet rs = stmt.executeQuery();
			return mapStakeholders(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving stakeholders by staName " + staName;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert den Stakeholder mit der übergebenen Id
	 * 
	 * @param staId
	 *            id des Stakeholder
	 * @return Stakeholder oder NULL
	 */
	public Stakeholder getByStaId(long staId) {
		log.trace("getByStaId " + staId);

		// TODO: There is an issue with this repository method. Find and fix it!
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select staId, staName, staDescription, comment from stakeholders where staId=?");
			stmt.setLong(1, staId);
			ResultSet rs = stmt.executeQuery();
			return mapStakeholders(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving stakeholders by staId " + staId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert den übergebenen stakeholder in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(Stakeholder i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update stakeholders set staName=?, staDescription=?, comment=? where staId=?");
			stmt.setString(1, i.getStaName());
			stmt.setString(2, i.getStaDescription());
			stmt.setString(3, i.getComment());
			stmt.setLong(4, i.getStaId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating stakeholders " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Löscht den Stakeholder mit der angegebenen Id von der DB
	 * 
	 * @param id
	 *            Stakeholder ID
	 */
	public void delete(long staId) {
		log.trace("delete " + staId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from stakeholders where staId=?");
			stmt.setLong(1, staId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing stakeholders by staId " + staId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert den angegebenen Stakeholder in der DB. INSERT.
	 * 
	 * @param i
	 *            neu zu erstellenden Stakeholder
	 * @return Liefert die von der DB generierte id des neuen Stakeholder zurück
	 */
	public long insert(Stakeholder i) {

		log.trace("insert " + i);

		// Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into stakeholders (staName, staDescription, comment) values (?,?,?)");
			stmt.setString(1, i.getStaName());
			stmt.setString(2, i.getStaDescription());
			stmt.setString(3, i.getComment());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long staId = key.getLong(1);
			return staId;
		} catch (SQLException e) {
			String msg = "SQL error while updating stakeholders " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Helper zum konvertieren der Resultsets in Stakeholder-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @author Marcel Briggen
	 * @throws SQLException
	 *
	 */
	private static Collection<Stakeholder> mapStakeholders(ResultSet rs) throws SQLException {
		LinkedList<Stakeholder> list = new LinkedList<Stakeholder>();
		while (rs.next()) {
			Stakeholder i = new Stakeholder(
					rs.getLong("staId"), 
					rs.getString("staName"), 
					rs.getString("staDescription"),
					rs.getString("comment"));
			list.add(i);
		}
		return list;
	}

}
