package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Vorname und Nachname (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Location {

	private long locId;
	private String locName;
	private long zipcode;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Location() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige Id
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public Location(long locId, String locName, long zipcode) {
		this.locId = locId;
		this.locName = locName;
		this.zipcode = zipcode;
	}

	public String getLocName() {
		return locName;
	}

	public void setLocName(String locName) {
		this.locName = locName;
	}

	public long getLocId() {
		return locId;
	}

	public void setLocId(long locId) {
		this.locId = locId;
	}

	public long getZipcode() {
		return zipcode;
	}

	public void setZipcode(long zipcode) {
		this.zipcode = zipcode;
	}

	@Override
	public String toString() {
		return String.format("Locaation:{locId: %d, locName: %s, zipcode: %d}", locId, locName, zipcode);
	}

}
