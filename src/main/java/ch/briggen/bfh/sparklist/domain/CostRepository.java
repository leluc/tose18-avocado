package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Costs. 
 * Hier werden alle Funktionen für die DB-Operationen zu Costs implementiert
 * @author Marcel Briggen
 *
 */


public class CostRepository {
	
	private final Logger log = LoggerFactory.getLogger(CostRepository.class);
	

	/**
	 * Liefert alle Costs in der Datenbank
	 * @return Collection aller Costs
	 */
	public Collection<Cost> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select costId, budIdFk, corpIdFk, amount, fallingdue from costs");
			ResultSet rs = stmt.executeQuery();
			return mapCosts(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all costs. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Costs mit dem angegebenen Amount
	 * @param name
	 * @return Collection mit dem Amount "amount"
	 */
	public Collection<Cost> getByAmount(int amount) {
		log.trace("getByAmount " + amount);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select costId, budIdFk, corpIdFk, amount, fallingdue from costs where amount=?");
			stmt.setLong(1, amount);
			ResultSet rs = stmt.executeQuery();
			return mapCosts(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving costs by amount " + amount;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Cost mit der übergebenen Id
	 * @param id id des Project
	 * @return Project oder NULL
	 */
	public Cost getByCostId(long costId) {
		log.trace("getByCostId " + costId);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select costId, budIdFk, corpIdFk, amount, fallingdue from costs where costId=?");
			stmt.setLong(1, costId);
			ResultSet rs = stmt.executeQuery();
			return mapCosts(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by costId " +costId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Budget in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Cost i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update costs set costId, budIdFk, corpIdFk, amount, fallingdue from costs where costId=?");
			stmt.setLong(1, i.getCostId());
			stmt.setLong(2, i.getBudIdFk());
			stmt.setLong(3, i.getCorpIdFk());
			stmt.setInt(4, i.getAmount());
			stmt.setString(5, i.getFallingdue());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating costs " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Cost mit der angegebenen Id von der DB
	 * @param id Budget ID
	 */
	public void delete(long costId) {
		log.trace("delete " + costId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from costs where costId=?");
			stmt.setLong(1, costId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing costs by id " + costId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Cost in der DB. INSERT.
	 * @param i neu zu erstellendes Cost
	 * @return Liefert die von der DB generierte id des neuen Costs zurück
	 */
	public long insert(Cost i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into costs (costId, budIdFk, corpIdFk, amount, fallingdue) values (?,?,?,?,?)");
			stmt.setLong(1, i.getCostId());
			stmt.setLong(2, i.getBudIdFk());
			stmt.setLong(3, i.getCorpIdFk());
			stmt.setInt(4, i.getAmount());
			stmt.setString(5, i.getFallingdue());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long budId = key.getLong(1);
			return budId;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating costs " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Cost-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Cost> mapCosts(ResultSet rs) throws SQLException 
	{
		LinkedList<Cost> list = new LinkedList<Cost>();
		while(rs.next())
		{
			Cost i = new Cost(
					rs.getLong("costId"),
					rs.getLong("budIdFk"),
					rs.getLong("corpIdFk"),
					rs.getInt("amount"),
					rs.getString("fallingdue"));
			
			list.add(i);
		}
		return list;
	}

}
