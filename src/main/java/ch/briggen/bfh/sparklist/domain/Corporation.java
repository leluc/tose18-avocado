package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Vorname und Nachname (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Corporation {

	private long corpId;
	private int corpNumber;
	private String corpName;
	private String streetName;
	private int streetNumber;
	private long locIdFk;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Corporation() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige Id
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public Corporation(long corpId, int corpNumber, String corpName, String streetName, int streetNumber, long locIdFk) {
		this.corpId = corpId;
		this.corpNumber = corpNumber;
		this.corpName = corpName;
		this.streetName = streetName;
		this.streetNumber = streetNumber;
		this.locIdFk = locIdFk;
	}

	public int getCorpNumber() {
		return corpNumber;
	}

	public void setCorpNumber(int corpNumber) {
		this.corpNumber = corpNumber;
	}

	public String getCorpName() {
		return corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public long getCorpId() {
		return corpId;
	}

	public void setCorpId(long corpId) {
		this.corpId = corpId;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	
	public int getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(int streetNumber) {
		this.streetNumber = streetNumber;
	}
	
	public long getLocIdFk() {
		return locIdFk;
	}

	public void setLocIdFk(long locIdFk) {
		this.locIdFk = locIdFk;
	}
	
	@Override
	public String toString() {
		return String.format("Corporation:{corpId: %d, corpNumber: %d, corpName: %s, streetName: %s, streetNumber: %d, locIdFk: %d}", corpId, corpNumber, corpName, streetName, streetNumber, locIdFk);
	}

}
