package ch.briggen.bfh.sparklist.domain;

/**
 * 
 * @author Marcel Briggen
 *
 */
public class ProjectManagement {

	private long manId;
	private long manPriority;
	private long repIdFk;
	private long budIdFk;
	private long teamIdFk;
	

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public ProjectManagement() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige Id
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public ProjectManagement(long manId, long manPriority, long repIdFk, long budIdFk, long teamIdFk) {
		this.manId = manId;
		this.manPriority = manPriority;
		this.repIdFk = repIdFk;
		this.budIdFk = budIdFk;
		this.teamIdFk = teamIdFk;
	}

	public long getManId() {
		return manId;
	}

	public void setManId(long manId) {
		this.manId = manId;
	}

	public long getManPriority() {
		return manPriority;
	}

	public void setManPriority(long manPriority) {
		this.manPriority = manPriority;
	}
	
	public long getRepIdFk() {
		return repIdFk;
	}

	public void setRepIdFk(long repIdFk) {
		this.repIdFk = repIdFk;
	}
	
	public long getBudIdFk() {
		return budIdFk;
	}

	public void setBudIdFk(long budIdFk) {
		this.budIdFk = budIdFk;
	}
	
	public long getTeamIdFk() {
		return teamIdFk;
	}

	public void setTeamIdFk(long teamIdFk) {
		this.teamIdFk = teamIdFk;
	}
	
	@Override
	public String toString() {
		return String.format("ProjectManagement:{manId: %d; manPriority: %d; repIdFk: %d; budIdFk: %d; teamIdFk: %d}",
				manId, manPriority, repIdFk, budIdFk, teamIdFk);
	}

}
