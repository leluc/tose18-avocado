package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Vorname und Nachname (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Employee {

	private long empId;
	private String prename;
	private String lastname;
	private String empRole;
	private long depIdFk;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Employee() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige Id
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public Employee(long empId, String prename, String lastname, String empRole, long depIdFk) {
		this.empId = empId;
		this.prename = prename;
		this.lastname = lastname;
		this.empRole = empRole;
		this.depIdFk = depIdFk;
	}

	public String getPrename() {
		return prename;
	}

	public void setPrename(String prename) {
		this.prename = prename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public long getEmpId() {
		return empId;
	}

	public void setEmpId(long empId) {
		this.empId = empId;
	}

	public String getEmpRole() {
		return empRole;
	}

	public void setEmpRole(String empRole) {
		this.empRole = empRole;
	}
	
	public long getDepIdFk() {
		return depIdFk;
	}

	public void setDepIdFk(long depIdFk) {
		this.depIdFk = depIdFk;
	}
	
	@Override
	public String toString() {
		return String.format("Employee:{empId: %d; prename: %s; lastname: %s; empRole: %s; depIdFk: %d;}", empId, prename, lastname, empRole, depIdFk);
	}

}
