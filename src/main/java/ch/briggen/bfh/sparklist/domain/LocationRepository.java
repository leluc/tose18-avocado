package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Corporation. Hier werden alle Funktionen für die
 * DB-Operationen zu Corporation implementiert
 * 
 * @author Marcel Briggen
 *
 */

public class LocationRepository {

	private final Logger log = LoggerFactory.getLogger(LocationRepository.class);

	/**
	 * Liefert alle Location in der Datenbank
	 * 
	 * @return Location aller Corporation
	 */
	public Collection<Location> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select locId, locName, zipcode from locations");
			ResultSet rs = stmt.executeQuery();
			return mapLocations(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all locations. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Location mit dem angegebenen Namen
	 * 
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Location> getByLocName(String locName) {
		log.trace("getByLocName " + locName);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select locId, zipcode from locations where locName=?");
			stmt.setString(1, locName);
			ResultSet rs = stmt.executeQuery();
			return mapLocations(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving locations by locName " + locName;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Liefert das Corporation mit der übergebenen Id
	 * 
	 * @param id
	 *            id des Location
	 * @return Employee oder NULL
	 */
	public Location getByLocId(long locId) {
		log.trace("getByLocId " + locId);

		// TODO: There is an issue with this repository method. Find and fix it!
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select locId, locName, zipcode from locations where locId=?");
			stmt.setLong(1, locId);
			ResultSet rs = stmt.executeQuery();
			return mapLocations(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving locations by locId " + locId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das übergebene Location in der Datenbank. UPDATE.
	 * 
	 * @param i
	 */
	public void save(Location i) {
		log.trace("save " + i);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("update locations set locName=?, zipcode=? where locId=?");
			stmt.setString(1, i.getLocName());
			stmt.setLong(2, i.getZipcode());
			stmt.setLong(3, i.getLocId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating locations " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Löscht das Employee mit der angegebenen Id von der DB
	 * 
	 * @param id
	 *            Location ID
	 */
	public void delete(long locId) {
		log.trace("delete " + locId);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from locations where locId=?");
			stmt.setLong(1, locId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while deleteing locations by locId " + locId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * Speichert das angegebene Location in der DB. INSERT.
	 * 
	 * @param i
	 *            neu zu erstellendes Location
	 * @return Liefert die von der DB generierte id des neuen Corporation zurück
	 */
	public long insert(Location i) {

		log.trace("insert " + i);

		// Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("insert into locations (locName, zipcode) values (?,?)");
			stmt.setString(1, i.getLocName());
			stmt.setLong(2, i.getZipcode());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long locId = key.getLong(1);
			return locId;
		} catch (SQLException e) {
			String msg = "SQL error while updating locations " + i;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}
	


	/**
	 * Helper zum konvertieren der Resultsets in Corporation-Objekte. Siehe getByXXX
	 * Methoden.
	 * 
	 * @author Marcel Briggen
	 * @throws SQLException
	 *
	 */
	private static Collection<Location> mapLocations(ResultSet rs) throws SQLException{
		LinkedList<Location> list = new LinkedList<Location>();
		while (rs.next()) {
			Location i = new Location(
					rs.getLong("locId"),
					rs.getString("locName"),
					rs.getLong("zipcode"));
			list.add(i);
		}
		return list;
	}

}
