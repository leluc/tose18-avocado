package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Projekt-ID, Projektname, Projektmanager und Beschreibung (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class ProjectTeam {

	private long teamId;
	private long proIdFk;
	private long empIdFk;
	
	public ProjectTeam() {

	}

	public ProjectTeam(long teamId, long proIdFk, long empIdFk) {
		this.teamId = teamId;
		this.proIdFk = proIdFk;
		this.empIdFk = empIdFk;
	}

	public long getTeamId() {
		return teamId;
	}

	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}
	
	public long getProIdFk() {
		return proIdFk;
	}

	public void setProIdFk(long proIdFk) {
		this.proIdFk = proIdFk;
	}

	public long getEmpIdFk() {
		return empIdFk;
	}

	public void setEmpId(long empIdFk) {
		this.empIdFk = empIdFk;
	}
	
	public String toString() {
		return String.format("ProjectTeam:{teamId: %d; proIdFk: %d; empIdFk: %d;}",
				teamId, proIdFk, empIdFk);
	}
	
}
