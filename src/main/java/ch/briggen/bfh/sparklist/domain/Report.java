package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Projekt-ID, Projektname, Projektmanager und Beschreibung (und einer eindeutigen Id)
 * 
 * @author Marcel Briggen
 *
 */
public class Report {

	private long repId;
	private String repName;
	private long proIdFk;
	private long corpIdFk;
	private String repGoal;
	private String repDescription;
	private long budIdFk;
	private long teamIdFk;
	private long staIdFk;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Report() {

	}

	/**
	 * Konstruktor
	 * 
	 * @param id
	 *            Eindeutige Id
	 * @param name
	 *            Name des eintrags in der Liste
	 * @param quantity
	 *            Menge
	 */
	public Report(long repId, String repName, long proIdFk, long corpIdFk, String repGoal, String repDescription, long budIdFk, long teamIdFk, long staIdFk) {
		this.repId = repId;
		this.repName = repName;
		this.proIdFk = proIdFk;
		this.corpIdFk = corpIdFk;
		this.repGoal = repGoal;
		this.repDescription = repDescription;
		this.budIdFk = budIdFk;
		this.teamIdFk = teamIdFk;
		this.staIdFk = staIdFk;
	}

	public long getRepId() {
		return repId;
	}

	public void setRepId(long repId) {
		this.repId = repId;
	}

	public long getProIdFk() {
		return proIdFk;
	}

	public void setProIdFk(long proIdFk) {
		this.proIdFk = proIdFk;
	}

	public long getCorpIdFk() {
		return corpIdFk;
	}

	public void setCorpIdFk(long corpIdFk) {
		this.corpIdFk = corpIdFk;
	}
	
	public String getRepGoal() {
		return repGoal;
	}

	public void setRepGoal(String repGoal) {
		this.repGoal = repGoal;
	}
	
	public String getRepDescription() {
		return repDescription;
	}

	public void setRepDescription(String repDescription) {
		this.repDescription = repDescription;
	}

	public long getBudIdFk() {
		return budIdFk;
	}

	public void setBudIdFk(long budIdFk) {
		this.budIdFk = budIdFk;
	}
	
	public long getTeamIdFk() {
		return teamIdFk;
	}

	public void setTeamIdFk(long teamIdFk) {
		this.teamIdFk = teamIdFk;
	}
	
	public long getStaIdFk() {
		return staIdFk;
	}

	public void setStaIdFk(long staIdFk) {
		this.staIdFk = staIdFk;
	}

	@Override
	public String toString() {
		return "Report [repId=" + repId + ", repName=" + repName + ", proIdFk=" + proIdFk + ", corpIdFk=" + corpIdFk
				+ ", repGoal=" + repGoal + ", repDescription=" + repDescription + ", budIdFk=" + budIdFk + ", teamIdFk="
				+ teamIdFk + ", staIdFk=" + staIdFk + "]";
	}

	public String getRepName() {
		return repName;
	}

	public void setRepName(String repName) {
		this.repName = repName;
	}

}
