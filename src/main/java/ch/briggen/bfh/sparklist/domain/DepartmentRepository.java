package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projects. 
 * Hier werden alle Funktionen für die DB-Operationen zu Projects implementiert
 * @author Marcel Briggen
 *
 */


public class DepartmentRepository {
	
	private final Logger log = LoggerFactory.getLogger(DepartmentRepository.class);
	

	/**
	 * Liefert alle departments in der Datenbank
	 * @return Collection aller Departments
	 */
	public Collection<Department> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select depId, depName from departments");
			ResultSet rs = stmt.executeQuery();
			return mapDepartments(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all departments. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Projects mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Department> getByDepName(long depName) {
		log.trace("getByDepName " + depName);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select depId, depName from departments where depName=?");
			stmt.setLong(1, depName);
			ResultSet rs = stmt.executeQuery();
			return mapDepartments(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving departments by depName " + depName;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Project mit der übergebenen depId
	 * @param id id des Project
	 * @return Project oder NULL
	 */
	public Department getByDepId(long depId) {
		log.trace("getByDepId " + depId);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select depId, depName from departments where depId=?");
			stmt.setLong(1, depId);
			ResultSet rs = stmt.executeQuery();
			return mapDepartments(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving departments by depId " + depId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene project in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Department i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update departments set depName=? where depId=?");
			stmt.setString(1, i.getDepName());
			stmt.setLong(2, i.getDepId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating departments " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Project mit der angegebenen Id von der DB
	 * @param id Project ID
	 */
	public void delete(long depId) {
		log.trace("delete " + depId);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from departments where depId=?");
			stmt.setLong(1, depId);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing departments by depId " + depId;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Project in der DB. INSERT.
	 * @param i neu zu erstellendes Project
	 * @return Liefert die von der DB generierte id des neuen Projects zurück
	 */
	public long insert(Department i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into departments (depName) values (?)");
			stmt.setString(1, i.getDepName());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating departments " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Project-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Department> mapDepartments(ResultSet rs) throws SQLException 
	{
		LinkedList<Department> list = new LinkedList<Department>();
		while(rs.next())
		{
			Department i = new Department(
					rs.getLong("depId"),
					rs.getString("depName"));
			
			list.add(i);
		}
		return list;
	}

}
