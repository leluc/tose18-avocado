package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ItemRepositorySQLErrorTest {

	ItemRepository repo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		repo = new ItemRepository();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSQLExceptionGetAll() {
		assertThrows(RepositoryException.class, ()->{repo.getAll();});		
	}
	
	@Test
	void testSQLExceptionGetByName() {
		assertThrows(RepositoryException.class, ()->{repo.getByName("NotHere");});		
	}
	
	@Test
	void testSQLExceptionGetById() {
		assertThrows(RepositoryException.class, ()->{repo.getById(0);});		
	}
	
	@Test
	void testSQLExceptionInsert() {
		assertThrows(RepositoryException.class, ()->{repo.insert(null);});		
	}

	@Test
	void testSQLExceptionUpdate() {
		assertThrows(RepositoryException.class, ()->{repo.save(null);});		
	}
	
	@Test
	void testSQLExceptionDelete() {
		assertThrows(RepositoryException.class, ()->{repo.delete(0);});		
	}
}
